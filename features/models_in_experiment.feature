Feature: dFBA sample
	Dynamically sample FBA models model

Scenario: Load models into experiment
	Given YAML config file
	And networks
	When create experiment
	Then experiment contains matching model for every species
