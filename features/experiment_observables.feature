Feature: Experiment class
    Represent a population dynamics experiment

Scenario: Observables map
    Given an experiment
    When define substrate and output metabolites
    And assign substrate reactions for bacterium
    And assign output reactions for bacterium
    And assign biomass reaction for bacterium
    Then observables map metabolites to reactions
    And substrate reactions are correct
    And output reactions are correct
    And biomass reaction is correct
