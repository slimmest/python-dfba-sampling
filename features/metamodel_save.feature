Feature: Metamodel of FBA
    Metamodel for estimating the flux

Scenario: Save a metamodel and the figures of the learning
    Given a database
    And a learned metamodel
    When save the parameters and the figures
    Then metamodel parameters are saved
    And the figures are saved
