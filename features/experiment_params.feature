Feature: Experiment class
	Represent a population dynamics experiment

Scenario: Extract experiment parameters
	Given a YAML config file
	When create experiment object
	Then experiment has species
	And experiment has substrates
	And experiment has outputs
	And experiment has constants
	And experiment has derived constants
	And experiment has Y_id for species
	And experiment has Y_id for metabolites
