Feature: Database of dFBA observations
    Training database of observations obtained from dFBA experiments

Scenario: Invent new experiments
    Given a possibly empty set of initial experiment specifications
    And invention guideline parameters
    When run invent 2 new experiment specifications
    Then obtain 2 new experiment specifications
    And the 2 new experiment specifications involve same species
