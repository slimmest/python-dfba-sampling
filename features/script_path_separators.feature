Feature: dFBA sample
	Dynamically sample FBA models model

Scenario: Script directories without trailing slash
	Given config file
	And SBML model
	And output directory without trailing slash
	When run dfba_sampling
	Then saved experiment in correct directory
	And plot files in correct directory
