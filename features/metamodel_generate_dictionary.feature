Feature: Dictionary of metamodel
    Dictionary containing one metamodel per bacteria for estimating the flux

Scenario: generate the dictionary of metamodels
    Given a bacteria id
    And a database
    When a metamodel is learned
    And the results are saved
    And the dictionnary of metamodels is generated
    Then the dictionary contains one element
    And the keyword of this element is the bacteria
    And this element is object of the class Metamodel
