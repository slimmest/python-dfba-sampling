Feature: dFBA sample
	Dynamically sample one model

Scenario: Sample Salmonella typhimurium (iRR1083)
	Given config file
	And SBML model
	And output directory
	When run dfba_sampling
	Then have tabular files
	And have plot files
	And they match ground truth

