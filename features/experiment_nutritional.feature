Feature: Experiment class
	Represent a population dynamics experiment

Scenario: Extract nutritional environment from parameters
	Given a YAML config file
	When create experiment object
	Then experiment has expected species
	And experiment maps species to substrate_reaction
	And experiment maps species to output_reaction
	And experiment maps metabolite to species
