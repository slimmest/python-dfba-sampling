Feature: Database of dFBA observations
    Training database of observations obtained from dFBA experiments

Scenario: Explore sampling space
    Given a possibly empty set of initial experiment specifications
    And invention guideline parameters
    When invent and run 2 new experiments
    And build sample database from experiments
    Then obtain a sequence of observations
    And sequence has expected dimensions
