Feature: Experiment class
	Represent a population dynamics experiment

Scenario: Extract experiment parameters
	Given a YAML config file
	When create experiment object
	Then experiment has required models
