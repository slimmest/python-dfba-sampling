Feature: dFBA sample
    Dynamically sample one model with lexicographic FBA

    Scenario: dFBA with lexicographic FBA works
        Given config file for working dFBA with lexicograpic FBA
        When create experiment object
        And run dfba_sampling 
        Then flux is not zero
        And output log does not report infeasible
        
    Scenario: dFBA with lexicographic FBA fails
        Given config file for failing dFBA with lexicograpic FBA
        When create experiment object
        And run dfba_sampling
        Then flux is zero
        And output log reports infeasible
