Feature: dFBA sample
    Dynamically sample one model

Scenario: Infeasible FBA
    Given file for infeasible config
    When create experiment object
    And run dfba_sampling
    Then flux is zero
    And output log reports infeasible
