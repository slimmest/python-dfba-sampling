Feature: Metamodel
    Represent a metamodel of Flux Balance Analysis

Scenario: Perform dFBA using metamodel instead of FBA
    Given an experiment
    And a database
    When a metamodel is learned
    And a dict of metamodels is generated
    And TimeIntegration is performed with FBA
    And TimeIntegration is performed with metamodel
    Then results have the same shape
