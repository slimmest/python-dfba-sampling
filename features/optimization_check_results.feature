Feature: Optimization
    Optimization process

Scenario: Optimize metamodel coefficients
    Given a database
    When run optimization
    Then optimization stopped
    And coefficients have expected shape
