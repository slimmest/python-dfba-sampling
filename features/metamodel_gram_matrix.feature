Feature: Metamodel of FBA
    Metamodel

Scenario: Generate gram matrix
    Given a database
    When initialize metamodel object
    Then the expected subsets are computed
    And the amount of computed matrices matches the number of subsets
    And the matrices all are symetric and have expected shape
