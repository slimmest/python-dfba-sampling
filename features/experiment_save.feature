Feature: Experiment class
    Represent a population dynamics experiment

Scenario: Save an experiment and reload it
    Given an experiment
    And a YAML save file
    When save experiment to file
    And reload experiment from file
    Then reloaded experiment is equal to original
