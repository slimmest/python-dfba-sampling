Feature: Database of dFBA observations
    Training database of observations obtained from dFBA experiments

Scenario: Build database from experiments
    Given a set of 2 experiments
    When run 2 time integration simulations
    And extract observations
    Then obtain a dfba result for each of the 2 experiments
    And obtain a sequence of 2 observations
