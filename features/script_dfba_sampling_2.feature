Feature: dFBA sample
	Dynamically sample one model with two bacterias

Scenario: Sample Salmonella typhimurium (iRR1083) and Faecalibacterium Prauznitzii
	Given config file
	And Two SBML models
	And output directory
	When run dfba_sampling
	Then have tabular files
	And have plot files
	And they match ground truth
