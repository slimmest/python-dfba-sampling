Feature: dFBA sample
	Dynamically sample FBA models model

Scenario: Check output directory
	Given invalid output directory
	And existing output directory
	And new output directory
	Then existing dir is assured
	And new dir is created and assured
	And invalid dir raises exception
