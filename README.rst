========
Overview
========

Dynamic FBA sampling for RKHS metamodeling

* Free software: GNU Lesser General Public License v3 or later (LGPLv3+)

Installation
============

You can install the in-development version with::

    pip install git+ssh://git@gitlab.inria.fr/Slimmest/python-dfba-sampling.git@master

Documentation
=============


https://python-dfba-sampling.readthedocs.io/


Development
===========

* Installing tox and python-dfba-sampling *

First install a virtual environment with the correct python named TOX and load it::

    virtualenv --python=python3.8 TOX
    source TOX/bin/activate

Now, tox can be installed and launched::

    pip install tox
    tox

In particular, it will install a virtualenv py38 with a correct installation of python-dfba and will test the installation by running tests. This virtual environment can be further loaded with::

    source <Path_to_virtual_env_tox>/TOX/bin/activate
    source <Path_to_py38_virtual_env>/py38/bin/activate

Any modification to python-dfba-sampling can be installed and tested by running::

    tox

The virtual environment py38 will contain the up-to-date version of the package that can be used by activating py38.

*Using tox*

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
