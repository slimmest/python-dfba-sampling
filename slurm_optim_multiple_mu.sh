#!/bin/bash

# Submit the default workflow
#
#   sbatch slurm_multiple_reg.sh
#

#resource requests are prefixed with #SBATCH
#SBATCH -J SLIMMEST
#SBATCH -c 24
#SBATCH --mem 20G
#SBATCH --mail-type END # receive an email when job is over
#SBATCH -t 24:00:00

# Assume we are in repo directory unless SLIMMEST_HOME is set
echo Job running from ${SLIMMEST_HOME:=$PWD}
: ${SLIMMEST_ENV:=${SLIMMEST_HOME}/.tox/py39}

# Load py39 virtualenv from tox workdir
[ -s ${SLIMMEST_ENV}/bin/activate ] && source ${SLIMMEST_ENV}/bin/activate
if ! command -v dfba-sampling > /dev/null; then
    module add language/python/3.9
    tox -e py39 --notest
    source .tox/py39/bin/activate
fi

output="../expy060"
for d in 0 0.001 0.01 0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.75 1
# for n in 100 200 500 900 1200 1500 1800 2100
do
    # Run step concurrently in background
    srun \
        --label \
        --input none \
        --output data/srun-${SLURM_JOBID:-pid.$$}.log \
        python3 src/dfba_sampling/optimization_main.py --data $output/constraints_2100_train.tsv d$output/fluxes_styphi_2100_train.tsv --output $output/styphi --gram $output/gram_matrix_2100.npz --params "{'mu': '$d', 'learning_rate': '2e-6', 'n_iter': '3', 'n_obs': '2100', 'eps_res': '1e-4'}" \
        &
done

# Job must wait until all backgrounded steps are finished
wait
