#!/bin/bash

# Submit the default workflow
#
#   sbatch slurm.sh
#
# or specifiy a job specifically
#
#   sbatch slurm.sh --config experiment.yml
#

#resource requests are prefixed with #SBATCH
#SBATCH -J SLIMMEST
#SBATCH -c 24
#SBATCH --mem 20G
#SBATCH --mail-type END # receive an email when job is over
#SBATCH -t 2:00:00

# Assume we are in repo directory unless SLIMMEST_HOME is set
echo Job running from ${SLIMMEST_HOME:=$PWD}
: ${SLIMMEST_ENV:=${SLIMMEST_HOME}/.tox/py39}

# Load py39 virtualenv from tox workdir
[ -s ${SLIMMEST_ENV}/bin/activate ] && source ${SLIMMEST_ENV}/bin/activate
if ! command -v dfba-sampling > /dev/null; then
    module add language/python/3.9
    tox -e py39 --notest
    source .tox/py39/bin/activate
fi

# Handle each config to run
dir=$1
for candidate in $1/*; do
    if [[ "$candidate" == --* ]]; then
	dfba-sampling ${@}
	break
    elif [ -d "$candidate" ]; then
	config=${candidate%/}/config.yml
	output=${candidate%/}
    else
	config=${candidate}
	output=$(mktemp -d -p $PWD dfba-XXXXXXXX.d)
	rsync ${candidate} ${output}/config.yml
    fi

    set -x

    # Run step concurrently in background
    srun \
	--label \
	--input none \
	--output ${output}/srun-${SLURM_JOBID:-pid.$$}.log \
	dfba-sampling --config=${config} --output=${output}/ \
	&

    set +x
done

# Job must wait until all backgrounded steps are finished
wait
