"""dFBA sample feature tests."""

from pathlib import Path  # noqa: F401

import pytest
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then

from dfba_sampling.utils import assure_valid_directory


@scenario("check_output_directory.feature", "Check output directory")
def test_check_output_directory():
    """Check output directory."""


@given("existing output directory", target_fixture="existing_output_directory")
def existing_output_directory():
    """existing output directory."""
    return Path("/tmp")


@given("invalid output directory", target_fixture="invalid_output_directory")
def invalid_output_directory():
    """invalid output directory."""
    return Path("/dev/null/impossible")


@given("new output directory", target_fixture="new_output_directory")
def new_output_directory(tmpdir):
    """new output directory."""
    return Path(tmpdir) / "output"


@then("existing dir is assured")
def existing_dir_is_assured(existing_output_directory):
    """existing dir is assured."""
    assure_valid_directory(str(existing_output_directory))
    assert existing_output_directory.exists()


@then("new dir is created and assured")
def new_dir_is_created_and_assured(new_output_directory):
    """new dir is created and assured."""
    assert not new_output_directory.exists()
    assure_valid_directory(str(new_output_directory))
    assert new_output_directory.exists()


@then("invalid dir raises exception")
def invalid_dir_raises_exception(invalid_output_directory):
    """invalid dir raises exception."""
    with pytest.raises(OSError) as e:
        assure_valid_directory(str(invalid_output_directory))
    assert e.type == NotADirectoryError
    assert not invalid_output_directory.exists()
