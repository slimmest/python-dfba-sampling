"""Experiment class feature tests."""

from pathlib import Path  # noqa: F401

import cobra
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.experiment import ExperimentSpec


@scenario("experiment_models.feature", "Extract experiment parameters")
def test_extract_experiment_parameters():
    """Extract experiment parameters."""


@given("a YAML config file", target_fixture="yaml_config")
def a_yaml_config_file():
    """a YAML config file."""
    return Path("tests/test_data") / "styphi_fprau.yml"


@when("create experiment object", target_fixture="experiment")
def create_experiment_object(yaml_config):
    """create experiment object."""
    return ExperimentSpec(config=yaml_config)


@then("experiment has required models")
def experiment_has_required_models(experiment):
    """experiment has required models."""
    assert set([m.id for m in experiment.models.values()]) == set(["Styphi", "Fprau"])
    for mod in experiment.models.values():
        assert isinstance(mod, cobra.core.Model)
