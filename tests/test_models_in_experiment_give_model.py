"""dFBA sample feature tests."""

from pathlib import Path  # noqa: F401

import cobra
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.create_dict import Create_parameter_dict_from_YAML


@scenario("models_in_experiment.feature", "Load models into experiment")
def test_load_models_into_experiment():
    """Load models into experiment."""


@given("YAML config file", target_fixture="yaml_config_file")
def yaml_config_file():
    """YAML config file."""
    return Path("tests/test_data/") / "styphi_no_model.yml"


@given("networks", target_fixture="networks")
def networks():
    """networks."""
    return [Path("data/") / "Salmonella_FBA.xml"]


@when("create experiment", target_fixture="experiment")
def create_experiment(yaml_config_file, networks):
    """create experiment."""
    param = Create_parameter_dict_from_YAML(
        str(yaml_config_file), [str(f) for f in networks]
    )
    return param


@then("experiment contains matching model for every species")
def experiment_contains_matching_model_for_every_species(experiment):
    """experiment contains matching model for every species."""
    for organism in experiment["species"]:
        model = experiment["model_dict"][organism]
        assert model is not None
        assert isinstance(model, cobra.core.Model)
