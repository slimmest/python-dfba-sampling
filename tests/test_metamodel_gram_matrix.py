"""dFBA sample feature tests."""

from pathlib import Path  # noqa: F401

import numpy as np
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.metamodel import Metamodel


@scenario("metamodel_gram_matrix.feature", "Generate gram matrix")
def test_generate_gram_matrix():
    """Generate gram matrix."""


@given("a database", target_fixture="database")
def a_database():
    "a database"
    return str(Path("./tests/test_data/learning_constraint.tsv")), str(
        Path("./tests/test_data/learning_flux.tsv")
    )


@when("initialize metamodel object", target_fixture="metamodel")
def initialize_metamodel_object(database):
    """initialize metamodel object"""
    mm = Metamodel(constraints=database[0], fluxes=database[1])
    mm.GramMatrix()
    return mm


@then("the expected subsets are computed")
def the_expected_subsets_are_computed(metamodel):
    """the expected subsets are computed"""
    truth = ((0,), (1,), (2,), (0, 1), (0, 2), (1, 2), (0, 1, 2))
    assert tuple(tuple(i) for i in metamodel.P) == truth


@then("the amount of computed matrices matches the number of subsets")
def the_amount_of_computed_matrices_matches_the_number_of_subsets(metamodel):
    """the amount of computed matrices matches the number of subsets"""
    assert metamodel.K.shape[0] == metamodel.Nv


@then("the matrices all are symetric and have expected shape")
def the_matrices_all_are_symetric_and_have_expected_shape(metamodel):
    """the matrices all are symetric and have expected shape"""
    for k in metamodel.K:
        assert np.array_equal(k, k.T)
    assert metamodel.K.shape == (metamodel.Nv, metamodel.Nobs, metamodel.Nobs)
