"""Experiment class feature tests."""

from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.experiment import ExperimentSpec


@scenario("experiment_observables.feature", "Observables map")
def test_observables_map():
    """Observables map."""


@given("an experiment", target_fixture="experiment")
def experiment():
    """an experiment."""
    return ExperimentSpec(species=["Bact"])


@when("define substrate and output metabolites")
def define_substrate_and_output_metabolites(experiment):
    """define substrate and output metabolites."""
    substrates = {
        "substrate_1": {"initial_value": 1},
        "substrate_2": {"initial_value": 2},
    }
    outputs = {"output_1": {"initial_value": 1}, "output_2": {"initial_value": 2}}
    experiment.add_substrate_metabolites(substrates)
    experiment.add_output_metabolites(outputs)


@when("assign substrate reactions for bacterium", target_fixture="substrate_rea")
def assign_substrate_rea_for_bacterium(experiment):
    """assign substrate reactions for bacterium."""
    substrate_reactions = {
        "Bact": {"substrate_1": "R_substrate_1", "substrate_2": "R_substrate_2"}
    }
    experiment.add_substrate_reaction(substrate_reactions)
    return substrate_reactions["Bact"]


@when("assign output reactions for bacterium", target_fixture="output_rea")
def assign_output_rea_for_bacterium(experiment):
    """assign output reactions for bacterium."""
    output_reactions = {"Bact": {"output_1": "R_output_1", "output_2": "R_output_2"}}
    experiment.add_output_reaction(output_reactions)
    return output_reactions["Bact"]


@when("assign biomass reaction for bacterium", target_fixture="biomass_rea")
def assign_biomass_rea_for_bacterium(experiment):
    """assign biomass reactions for bacterium."""
    biomass_reaction = {"Bact": "R_biomass_foo"}
    experiment.add_biomass_reaction(biomass_reaction)
    return biomass_reaction["Bact"]


@then("observables map metabolites to reactions")
def observables_map_metabolites_to_reactions(
    experiment, output_rea, substrate_rea, biomass_rea
):
    """observables map metabolites to reactions."""
    for met, reaction in experiment.observables("Bact"):
        assert (
            met in experiment.substrate_metabolites
            or met in experiment.output_metabolites
            or met in experiment.biomass
        )
        assert (
            reaction in substrate_rea.values()
            or reaction in output_rea.values()
            or reaction in biomass_rea
        )


@then("output reactions are correct")
def output_rea_are_correct(experiment):
    """output reactions are correct."""
    assert experiment._output_reaction["Bact"] is not {}


@then("substrate reactions are correct")
def substrate_reaoutput_reactions_are_correct(experiment):
    """substrate reactions are correct."""
    assert experiment._substrate_reaction["Bact"] is not {}


@then("biomass reaction is correct")
def biomass_reaction_is_correct(experiment):
    """biomass reaction is correct."""
    assert experiment._biomass_reaction["Bact"] is not {}
