"""dFBA sample feature tests."""

from pathlib import Path  # noqa: F401

import numpy as np
import pandas as pd
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

import dfba_sampling.cli


@scenario("script_dfba_sampling_1.feature", "Sample Salmonella typhimurium (iRR1083)")
def test_sample_salmonella_typhimurium_irr1083():
    """Sample Salmonella typhimurium (iRR1083)."""


@given("SBML model", target_fixture="sbml_model")
def sbml_model():
    """SBML model."""
    return "./data/Salmonella_FBA.xml"


@given("config file", target_fixture="config_file")
def config_file():
    """config file."""
    return "./tests/test_data/styphi.yml"


@given("output directory", target_fixture="output_directory")
def output_directory(tmpdir):
    """output directory."""
    return Path(tmpdir) / "output"


@when("run dfba_sampling")
def run_dfba_sampling(config_file, sbml_model, output_directory):
    """run dfba_sampling."""
    dfba_sampling.cli.main(
        [
            "-c",
            str(config_file),
            "-n",
            str(sbml_model),
            "-o",
            str(output_directory),
        ]
    )


@then("have tabular files")
def have_tabular_files(output_directory):
    """have tabular files."""
    for f in ["FBA_Styphi", "dFBA", "constraint"]:
        assert (output_directory / f"out_{f}.tsv").is_file()


@then("have plot files")
def have_plot_files(output_directory):
    """have tabular files."""
    for f in ["BacterialGrowth", "FBA"]:
        assert (output_directory / f"out_{f}.png").is_file()
        assert (output_directory / f"out_{f}.pdf").is_file()


@then("they match ground truth")
def they_match_ground_truth(output_directory):
    """they match ground truth."""
    truth_directory = Path(".") / "tests" / "test_data" / "Styphi_only"
    for f in ["FBA_Styphi", "dFBA", "constraint"]:
        result = pd.read_csv(output_directory / f"out_{f}.tsv", header=0, comment="#")
        truth = pd.read_csv(truth_directory / f"out_{f}.tsv", header=0, comment="#")

        if f == "dFBA":
            assert np.allclose(result.values[1:], truth.values[:-1])
        else:
            assert np.allclose(result, truth)
