"""dFBA sample feature tests."""

import re
from pathlib import Path  # noqa: F401

from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

import dfba_sampling.cli


@scenario("script_path_separators.feature", "Script directories without trailing slash")
def test_script_directories_without_trailing_slash():
    """Script directories without trailing slash."""


@given("SBML model", target_fixture="sbml_model")
def sbml_model():
    """SBML model."""
    return [Path("data") / f for f in ["Salmonella_FBA.xml"]]


@given("config file", target_fixture="config_file")
def config_file(tmpdir):
    """config file."""

    config_file = Path(tmpdir) / "config.yml"

    # Patch config file to run for only one time step
    with open(Path("tests") / "test_data" / "styphi.yml", "r") as source:
        with open(config_file, "w") as target:
            target.write(re.sub("^Time:.*", "Time: 1", source.read()))

    return config_file


@given("output directory without trailing slash", target_fixture="outdir_str")
def output_directory_without_trailing_slash(tmpdir):
    """output directory without trailing slash."""
    return str(Path(tmpdir) / "output")


@when("run dfba_sampling")
def run_dfba_sampling(config_file, sbml_model, outdir_str):
    """run dfba_sampling."""
    dfba_sampling.cli.main(
        [
            "-c",
            str(config_file),
            "-n",
            str(sbml_model),
            "-o",
            outdir_str,
        ]
    )


@then("saved experiment in correct directory")
def saved_experiment_in_correct_directory(outdir_str):
    """saved experiment in correct directory."""
    for f in ["FBA_Styphi", "dFBA", "constraint"]:
        assert (Path(outdir_str) / f"out_{f}.tsv").is_file()


@then("plot files in correct directory")
def plot_files_in_correct_directory(outdir_str):
    """plot files in correct directory."""
    for f in ["BacterialGrowth", "FBA"]:
        assert (Path(outdir_str) / f"out_{f}.png").is_file()
        assert (Path(outdir_str) / f"out_{f}.pdf").is_file()
