"""dFBA sample feature tests."""

import os
from pathlib import Path  # noqa: F401

from pytest_bdd import given
from pytest_bdd import parsers
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.dFBA import TimeIntegration
from dfba_sampling.experiment import ExperimentResult
from dfba_sampling.experiment import ExperimentSpec


@scenario("database_run_experiments.feature", "Build database from experiments")
def test_database_invent_experiments():
    """Build database from experiments."""


@given(
    parsers.parse("a set of {n_to_make:d} experiments"), target_fixture="experiments"
)
def set_of_experiments(n_to_make):
    """a set of experiments."""
    return [
        ExperimentSpec(config=Path("./tests/test_data/styphi_fprau.yml"))
        for _ in range(n_to_make)
    ]


@when(
    parsers.parse("run {n_to_make:d} time integration simulations"),
    target_fixture="results",
)
def run_time_integration_simulations(n_to_make, experiments):
    """run time integration simulations"""
    results = []
    for i in range(n_to_make):
        result_y, result_fba, result_constraint = TimeIntegration(experiments[i])
        results.append(
            ExperimentResult(experiments[i], result_y, result_constraint, result_fba)
        )
    return results


@when("extract observations")
def extract_observations(tmpdir, results):
    """extract observations."""
    for i, r in enumerate(results):
        os.mkdir(Path(tmpdir) / f"sample_{i}")
        r.save(Path(tmpdir) / f"sample_{i}")


@then(parsers.parse("obtain a dfba result for each of the {n_to_make:d} experiments"))
def obtain_a_dfba_result_for_each_experiment(n_to_make, tmpdir):
    """obtain a dfba result for each experiment"""
    for i in range(n_to_make):
        assert Path(tmpdir / f"sample_{i}/out_dFBA.tsv").is_file()


@then(parsers.parse("obtain a sequence of {n_to_make:d} observations"))
def obtain_a_sequence_of_observations(n_to_make, tmpdir):
    """new experiment specifications involve same species"""
    for i in range(n_to_make):
        assert Path(tmpdir / f"sample_{i}/out_dFBA.tsv").is_file()
        for s in ["Fprau", "Styphi"]:
            assert Path(tmpdir / f"sample_{i}/out_FBA_{s}.tsv").is_file()
