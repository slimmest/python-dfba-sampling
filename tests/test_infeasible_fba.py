"""dFBA sample feature tests."""

from pathlib import Path  # noqa: F401

import numpy as np
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.dFBA import Constraint_definition
from dfba_sampling.experiment import ExperimentSpec
from dfba_sampling.FBA import FBA_model_Wrapper


@scenario("infeasible_fba.feature", "Infeasible FBA")
def test_infeasible_fba():
    """Infeasible FBA."""


@given("file for infeasible config", target_fixture="config")
def file_for_infeasible_config():
    """file for infeasible config."""
    return str(Path("./tests/test_data/styphi_infeasible.yml"))


@when("create experiment object", target_fixture="experiment")
def create_experiment_object(config):
    """create experiment object."""
    return ExperimentSpec(config=config)


@when("run dfba_sampling", target_fixture="dfba_sampling")
def run_dfba_sampling(experiment, capsys):
    """run dfba_sampling."""

    # compute constraint at time t=0.
    constraint = Constraint_definition(experiment.y0, experiment)
    flux, flag_err = FBA_model_Wrapper(constraint, "Styphi", experiment)
    assert flag_err

    return flux, capsys.readouterr().out


@then("flux is zero")
def flux_is_zero(dfba_sampling):
    """flux is zero."""
    flux, _ = dfba_sampling
    assert not np.any(flux)


@then("output log reports infeasible")
def output_log_reports_infeasible(dfba_sampling):
    """output log reports infeasible."""
    _, log = dfba_sampling
    assert "infeasible in Styphi" in log
