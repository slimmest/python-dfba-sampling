"""Experiment class feature tests."""

from pathlib import Path  # noqa: F401

from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.experiment import ExperimentSpec


@scenario(
    "experiment_nutritional.feature", "Extract nutritional environment from parameters"
)
def test_extract_nutritional_environment_from_parameters():
    """Extract nutritional environment from parameters."""


@given("a YAML config file", target_fixture="yaml_config")
def a_yaml_config_file():
    """a YAML config file."""
    return Path("tests/test_data") / "styphi_fprau.yml"


@when("create experiment object", target_fixture="experiment")
def create_experiment_object(yaml_config):
    """create experiment object."""
    return ExperimentSpec(config=yaml_config)


@then("experiment has expected species")
def experiment_has_expected_species(experiment):
    """experiment has expected species."""
    assert set(experiment.species) == set(["Styphi", "Fprau"])


@then("experiment maps metabolite to species")
def experiment_maps_metabolite_to_species(experiment):
    """
    Experiment maps metabolite to species.
    formerly param["Substrate_Bacteria_Map"][metabolite] -> species
    """
    for met in experiment.substrate_metabolites:
        assert (
            experiment.substrate_species(met) is not None
        ), "substrate_species: metabolite -> species"


@then("experiment maps species to output_reaction")
def experiment_maps_species_to_output_reaction(experiment):
    """
    Experiment maps species to output_reaction.
    formerly param["Output_Reaction_Map"][species] -> metabolite -> reaction
    """
    for species in experiment.species:
        assert (
            experiment.output_reaction(species) is not None
        ), "output: species -> metabolite -> reaction"


@then("experiment maps species to substrate_reaction")
def experiment_maps_species_to_substrate_reaction(experiment):
    """
    Experiment maps species to substrate_reaction.
    formerly param["Substrate_Reaction_Map"][species] -> metabolite -> reaction
    """
    for species in experiment.species:
        assert (
            experiment.substrate_reaction(species) is not None
        ), "substrate: species -> metabolite -> reaction"
