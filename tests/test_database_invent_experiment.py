"""dFBA sample feature tests."""

from pathlib import Path  # noqa: F401

import yaml
from pytest_bdd import given
from pytest_bdd import parsers
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.constraint_sampler import Constraint_Sampler


@scenario("database_invent_experiments.feature", "Invent new experiments")
def test_database_invent_experiments():
    """Invent new experiments."""


@given(
    "a possibly empty set of initial experiment specifications",
    target_fixture="exp_config",
)
def set_of_initial_experiment_specifications():
    """possibly empty set of initial experiment specifications."""
    return str(Path("./tests/test_data/styphi_fprau.yml"))


@given("invention guideline parameters", target_fixture="sample_config")
def invention_guideline_parameters():
    """invention guideline parameters."""
    return str(Path("./tests/test_data/sampling_parameters.yml"))


@when(
    parsers.parse("run invent {n_to_make:d} new experiment specifications"),
    target_fixture="output_directory",
)
def run_invent_new_experiment_specifications(tmpdir, exp_config, sample_config):
    """run invent new experiment specifications"""
    cons = Constraint_Sampler(exp_config=exp_config, sample_config=sample_config)
    cons.param["path_to_directories"] = tmpdir
    cons.sample_yaml_files()
    return str(Path(tmpdir) / f"{cons.param['directories_prefix']}")


@then(parsers.parse("obtain {n_to_make:d} new experiment specifications"))
def obtain_new_experiment_specifications(n_to_make, output_directory):
    """obtain new experiment specifications"""
    for i in range(n_to_make):
        assert Path(str(output_directory) + f"_{i}/config.yml").is_file()


@then(
    parsers.parse(
        "the {n_to_make:d} new experiment specifications involve same species"
    )
)
def new_experiment_spefications_involve_same_species(n_to_make, output_directory):
    """new experiment specifications involve same species"""
    for i in range(n_to_make):
        with open(output_directory + f"_{i}/config.yml", "r") as stream:
            param = yaml.safe_load(stream)
            assert set(param["species"]) == set(["Styphi", "Fprau"])
