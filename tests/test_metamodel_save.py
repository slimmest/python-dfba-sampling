"""saving the metamodel and its learning output tests."""

from pathlib import Path  # noqa: F401

from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.metamodel import Metamodel
from dfba_sampling.utils import save_learning_output


@scenario("metamodel_save.feature", "Save a metamodel and the figures of the learning")
def test_save_a_metamodel_and_the_figures_of_the_learning():
    """Save a metamodel and the figures of the learning."""


@given("a database", target_fixture="database")
def a_database():
    "a database"
    return str(Path("./tests/test_data/learning_constraint.tsv")), str(
        Path("./tests/test_data/learning_flux.tsv")
    )


@given("a learned metamodel", target_fixture="metamodel")
def a_learned_metamodel(database):
    """a learned metamodel."""
    mm = Metamodel(constraints=database[0], fluxes=database[1], bact="bact")
    mm.GramMatrix()
    mm.learn_metamodel(0.01, iter_max=100, eps_res=1e-1, flag_print=False)
    return mm


@when("save the parameters and the figures")
def save_the_parameters_and_the_figures(metamodel, tmpdir):
    """save the parameters and the figures."""
    save_learning_output(metamodel, Path(tmpdir))


@then("metamodel parameters are saved")
def metamodel_parameters_are_saved(metamodel, tmpdir):
    """metamodel parameters are saved"""
    assert (Path(tmpdir) / f"metamodel_{metamodel.bact}_theta.npz").exists()


@then("the figures are saved")
def the_figures_are_saved(tmpdir):
    """the figures are saved."""
    path = Path(tmpdir)
    figs = ["loss", "groupLASSO_reg", "flux_comp_est_set", "flux_est_vs_set"]
    for f in figs:
        assert (path / str(f + ".pdf")).exists()
