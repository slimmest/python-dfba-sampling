"""Experiment class feature tests."""

from pathlib import Path  # noqa: F401

from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.experiment import ExperimentSpec


@scenario("experiment_save.feature", "Save an experiment and reload it")
def test_save_an_experiment_and_reload_it():
    """Save an experiment and reload it."""


@given("an experiment", target_fixture="original")
def an_experiment():
    """an experiment."""
    return ExperimentSpec(config=Path("tests/test_data") / "styphi_fprau.yml")


@given("a YAML save file", target_fixture="yaml_save_file")
def a_yaml_save_file(tmpdir):
    """a YAML save file."""
    return Path(tmpdir) / "save.yaml"


@when("save experiment to file")
def save_experiment_to_file(original, yaml_save_file):
    """save experiment to file."""
    original.save(str(yaml_save_file))


@when("reload experiment from file", target_fixture="reloaded")
def reload_experiment_from_file(yaml_save_file):
    """reload experiment from file."""
    return ExperimentSpec(config=str(yaml_save_file))


@then("reloaded experiment is equal to original")
def reloaded_experiment_is_equal_to_original(original, reloaded):
    """reloaded experiment is equal to original."""
    assert original == reloaded
