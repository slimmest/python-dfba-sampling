"""dFBA sample feature tests."""

from pathlib import Path  # noqa: F401

from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.metamodel import Metamodel


@scenario("optimization_check_results.feature", "Optimize metamodel coefficients")
def test_optimize_metamodel_coefficients():
    """Optimize metamodel coefficients."""


@given("a database", target_fixture="database")
def a_database():
    "a database"
    return str(Path("./tests/test_data/learning_constraint.tsv")), str(
        Path("./tests/test_data/learning_flux.tsv")
    )


@when("run optimization", target_fixture="metamodel")
def run_optimization(database):
    """run optimization"""
    mm = Metamodel(constraints=database[0], fluxes=database[1])
    mm.GramMatrix()
    mm.learn_metamodel(0.01, iter_max=100, eps_res=1e-1, flag_print=False)
    return mm


@then("optimization stopped")
def optimization_stopped(metamodel):
    """optimization stopped"""
    for i_compounds in range(metamodel.Ncompound):
        assert (
            len(metamodel.learning_loss[i_compounds]) == 101
            or metamodel.learning_loss[i_compounds][-1] <= 0.1
        )


@then("coefficients have expected shape")
def coefficients_have_expected_shape(metamodel):
    """coefficients have expected shape"""
    assert metamodel.theta.shape == (10, 7, 6)
    assert metamodel.theta_0.shape == (6,)
