"""Experiment class feature tests."""

from pathlib import Path  # noqa: F401

import numpy as np
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.experiment import ExperimentSpec


@scenario("experiment_params.feature", "Extract experiment parameters")
def test_extract_experiment_parameters():
    """Extract experiment parameters."""


@given("a YAML config file", target_fixture="yaml_config")
def a_yaml_config_file():
    """a YAML config file."""
    return Path("tests/test_data") / "styphi_fprau.yml"


@when("create experiment object", target_fixture="experiment")
def create_experiment_object(yaml_config):
    """create experiment object."""
    return ExperimentSpec(config=yaml_config)


@then("experiment has Y_id for metabolites")
def experiment_has_y_id_for_metabolites(experiment):
    """experiment has Y_id for metabolites."""
    assert experiment.y_id("glucose") == 0
    assert experiment.y_id("galactose") == 1
    assert experiment.y_id("thiosulfate") == 2
    assert experiment.y_id("butyrate") == 3


@then("experiment has Y_id for species")
def experiment_has_y_id_for_species(experiment):
    """experiment has Y_id for species."""
    assert experiment.y_id("Styphi") == 4
    assert experiment.y_id("Fprau") == 5


@then("experiment has constants")
def experiment_has_constants(experiment):
    """experiment has constants."""
    assert experiment.time == 5
    assert experiment.time_step == 0.1
    assert experiment.limit_dt == 0.5
    assert np.array_equal(
        experiment.intrinsic_flux, -5.0 * np.ones(len(experiment.compound_name))
    )


@then("experiment has derived constants")
def experiment_has_derived_constants(experiment):
    """experiment has derived constants."""
    assert (
        experiment.compound_name
        == experiment.substrate_metabolites
        + experiment.output_metabolites
        + experiment.species
    )
    assert experiment.y0.size == len(experiment.compound_name)


@then("experiment has outputs")
def experiment_has_outputs(experiment):
    """experiment has outputs."""
    assert experiment.output_metabolites == ["butyrate"]


@then("experiment has species")
def experiment_has_species(experiment):
    """experiment has species."""
    assert set(experiment.species) == set(["Styphi", "Fprau"])


@then("experiment has substrates")
def experiment_has_substrates(experiment):
    """experiment has substrates."""
    assert set(experiment.substrate_metabolites) == set(
        ["glucose", "galactose", "thiosulfate"]
    )
    # assert experiment.substrate_metabolites["galactose"] == {"initial_value": 76}
