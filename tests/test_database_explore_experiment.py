"""dFBA sample feature tests."""

from pathlib import Path  # noqa: F401

import pandas as pd
from pytest_bdd import given
from pytest_bdd import parsers
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.constraint_sampler import Constraint_Sampler
from dfba_sampling.dFBA import TimeIntegration
from dfba_sampling.experiment import ExperimentResult
from dfba_sampling.experiment import ExperimentSpec


@scenario("database_explore_parameters.feature", "Explore sampling space")
def test_database_explore_experiments():
    """Explore sampling space."""


@given(
    "a possibly empty set of initial experiment specifications",
    target_fixture="exp_config",
)
def set_of_initial_experiment_specifications():
    """possibly empty set of initial experiment specifications."""
    return str(Path("./tests/test_data/styphi_fprau.yml"))


@given("invention guideline parameters", target_fixture="sample_config")
def invention_guideline_parameters():
    """invention guideline parameters."""
    return str(Path("./tests/test_data/sampling_parameters.yml"))


@when(
    parsers.parse("invent and run {n_to_make:d} new experiments"),
    target_fixture="constraint_sampler",
)
def invent_and_run_new_experiments(exp_config, sample_config, n_to_make, tmpdir):
    """invent and run new experiments"""
    cons = Constraint_Sampler(exp_config=exp_config, sample_config=sample_config)
    cons.param["path_to_directories"] = tmpdir
    cons.sample_yaml_files()
    print(cons.__dict__)
    for i in range(n_to_make):
        print(i, "/", n_to_make)
        exp = ExperimentSpec(
            config=Path(tmpdir) / f"{cons.param['directories_prefix']}_{i}/config.yml"
        )
        result_y, result_fba, result_constraint = TimeIntegration(exp)
        result = ExperimentResult(exp, result_y, result_constraint, result_fba)
        print(result.__dict__)
        result.save(Path(tmpdir) / f"{cons.param['directories_prefix']}_{i}")
    return cons


@when("build sample database from experiments")
def build_sample_database_from_experiments(constraint_sampler):
    """build database from experiments."""
    constraint_sampler.assemble()


@then("obtain a sequence of observations")
def obtain_a_sequence_of_observations(tmpdir):
    """obtain a sequence of observations"""
    for f in ["constraints", "fluxes_Styphi", "fluxes_Fprau"]:
        assert Path(tmpdir + f"/database_{f}.tsv").is_file()


@then("sequence has expected dimensions")
def sequence_has_expected_dimensions(tmpdir, constraint_sampler):
    """sequence has expected dimensions"""
    for f, s in zip(["constraints", "fluxes_Styphi", "fluxes_Fprau"], [3, 6, 6]):
        db = pd.read_csv(str(Path(tmpdir + f"/database_{f}.tsv")), sep="\t")
        # assert db.shape == (constraint_sampler.database_x.shape[0], s)
        assert db.shape == (11, s)
