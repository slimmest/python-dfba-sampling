"""dFBA sample feature tests."""

from pathlib import Path  # noqa: F401

from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.dFBA import TimeIntegration
from dfba_sampling.experiment import ExperimentSpec
from dfba_sampling.metamodel import Metamodel
from dfba_sampling.metamodel import flux_metamodel_Wrapper
from dfba_sampling.utils import gen_dict_metamodel


@scenario("metamodel_dFBA.feature", "Perform dFBA using metamodel instead of FBA")
def test_perform_dFBA_using_metamodel_instead_of_FBA():
    """Perform dFBA using metamodel instead of FBA."""


@given("an experiment", target_fixture="experiment")
def an_experiment():
    """a set of experiments."""
    return ExperimentSpec(config=Path("./tests/test_data/styphi_fprau.yml"))


@given("a database", target_fixture="database")
def a_database():
    "a database"
    return str(Path("./tests/test_data/learning_constraint.tsv")), str(
        Path("./tests/test_data/learning_flux.tsv")
    )


@when("a metamodel is learned", target_fixture="mm")
def a_metamodel_is_learned(database, tmpdir):
    """a metamodel is learned."""
    mm = Metamodel(constraints=database[0], fluxes=database[1], bact="bact")
    mm.GramMatrix()
    mm.learn_metamodel(0.01, iter_max=100, eps_res=1e-1, flag_print=False)
    mm.save_results(Path(tmpdir) / "results.npz")
    return mm


@when("a dict of metamodels is generated", target_fixture="dict_mm")
def a_dict_of_metamodels_is_generated(mm, tmpdir):
    """a dict of metamodels is generated."""
    return gen_dict_metamodel(
        ["Styphi", "Fprau"],
        [mm, mm],
        [Path(tmpdir) / "results.npz", Path(tmpdir) / "results.npz"],
    )


@when("TimeIntegration is performed with FBA", target_fixture="results_FBA")
def timeintegration_is_performed_with_FBA(experiment):
    """TimeIntegration is performed with FBA."""
    return TimeIntegration(experiment)


@when("TimeIntegration is performed with metamodel", target_fixture="results_metamodel")
def timeintegration_is_performed_with_metamodel(experiment, dict_mm):
    """TimeIntegration is performed with metamodel."""
    return TimeIntegration(experiment, RHS=flux_metamodel_Wrapper, dict_mm=dict_mm)


@then("results have the same shape")
def results_have_the_same_shape(results_FBA, results_metamodel):
    """results have the same shape."""
    for r1, r2 in zip(results_FBA, results_metamodel):
        assert r1.shape == r2.shape
