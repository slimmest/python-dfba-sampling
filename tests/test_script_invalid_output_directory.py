"""dFBA sample feature tests."""

from pathlib import Path  # noqa: F401

import pytest
from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then

import dfba_sampling.cli


@scenario("script_invalid_output_directory.feature", "Script invalid output directory")
def test_script_invalid_output_directory():
    """Script invalid output directory."""


@given("SBML models", target_fixture="sbml_models")
def sbml_models():
    """SBML models."""
    return [Path("data") / f for f in ["Salmonella_FBA.xml"]]


@given("config file", target_fixture="config_file")
def config_file():
    """config file."""
    return Path("tests") / "test_data" / "styphi.yml"


@given("invalid output directory", target_fixture="output_directory")
def invalid_output_directory():
    """invalid output directory."""
    return Path("/dev/null/impossible")


@then("run script exits with failure")
def run_script_exits_with_failure(config_file, sbml_models, output_directory, capsys):
    """run script."""
    with pytest.raises(SystemExit) as e:
        param, args = dfba_sampling.cli.main(
            [
                "--config",
                str(config_file),
                "--output",
                str(output_directory),
                "--networks",
            ]
            + [str(p) for p in sbml_models]
        )

    assert e.type == SystemExit
    assert e.value.code == 1
    assert "Output directory invalid" in capsys.readouterr().out
