"""dFBA feature tests."""

from pathlib import Path  # noqa: F401

import numpy as np
from pytest_bdd import given
from pytest_bdd import parsers
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.dFBA import Constraint_definition
from dfba_sampling.experiment import ExperimentSpec
from dfba_sampling.FBA import FBA_model_Wrapper


@scenario("dFBA_with_lexicographic_FBA.feature", "dFBA with lexicographic FBA works")
def test_dFBA_with_lexicographic_FBA_works():
    """DFBA with lexicographic FBA works."""
    pass


@scenario("dFBA_with_lexicographic_FBA.feature", "dFBA with lexicographic FBA fails")
def test_dFBA_with_lexicographic_FBA_fails():
    """DFBA with lexicographic FBA fails."""
    pass


@given(
    parsers.parse("config file for {status} dFBA with lexicograpic FBA"),
    target_fixture="config",
)
def file_for_infeasible_config(status):
    """file for dFBA with lexicographic FBA config."""
    return str(
        Path("./tests/test_data/styphi_" + status + "_dfba_lexicographic_fba.yml")
    )


@when("create experiment object", target_fixture="experiment")
def create_experiment_object(config):
    """create experiment object."""
    return ExperimentSpec(config=config)


@when("run dfba_sampling", target_fixture="dfba_sampling")
def run_dfba_sampling(experiment, capsys):
    """run dfba_sampling."""

    # compute constraint at time t=0.
    constraint = Constraint_definition(experiment.y0, experiment)
    flux, flag_err = FBA_model_Wrapper(constraint, "Styphi", experiment)
    #    assert flag_err

    return flux, capsys.readouterr().out


@then("flux is not zero")
def flux_is_not_zero(dfba_sampling):
    """flux is zero."""
    flux, _ = dfba_sampling
    assert np.any(flux)


@then("flux is zero")
def flux_is_zero(dfba_sampling):
    """flux is zero."""
    flux, _ = dfba_sampling
    assert not np.any(flux)


@then("output log does not report infeasible")
def output_log_does_not_report_infeasible(dfba_sampling):
    """output log does not report infeasible."""
    _, log = dfba_sampling
    assert "Warning: lexicographic computation failed" not in log


@then("output log reports infeasible")
def output_log_reports_infeasible(dfba_sampling):
    """output log reports infeasible."""
    _, log = dfba_sampling
    assert "Warning: lexicographic computation failed" in log
