"""saving the metamodel and its learning output tests."""

from pathlib import Path  # noqa: F401

from pytest_bdd import given
from pytest_bdd import scenario
from pytest_bdd import then
from pytest_bdd import when

from dfba_sampling.metamodel import Metamodel
from dfba_sampling.utils import gen_dict_metamodel


@scenario(
    "metamodel_generate_dictionary.feature", "generate the dictionary of metamodels"
)
def test_generate_the_dictionnary_of_metamodels():
    """generate the dictionnary of metamodels."""


@given("a bacteria id", target_fixture="bacteria_id")
def a_bacteria_id():
    """a bacteria id."""
    return "bacteria_id"


@given("a database", target_fixture="database")
def a_database():
    """a database"""
    return str(Path("./tests/test_data/learning_constraint.tsv")), str(
        Path("./tests/test_data/learning_flux.tsv")
    )


@when("a metamodel is learned", target_fixture="mm")
def a_metamodel_is_learned(database, tmpdir):
    """a metamodel is learned."""
    mm = Metamodel(constraints=database[0], fluxes=database[1])
    mm.GramMatrix()
    mm.learn_metamodel(0.01, iter_max=100, eps_res=1e-1, flag_print=False)
    return mm


@when("the results are saved")
def the_results_are_saved(mm, tmpdir):
    """the results are saved"""
    mm.save_results(Path(tmpdir) / "results.npz")


@when("the dictionnary of metamodels is generated", target_fixture="mm_dict")
def the_dictionnary_of_metamodels_is_generated(bacteria_id, mm, tmpdir):
    """the dictionnary of metamodels is generated."""
    return gen_dict_metamodel([bacteria_id], [mm], [Path(tmpdir) / "results.npz"])


@then("the dictionary contains one element")
def the_dictionary_contains_one_element(mm_dict):
    """the dictionary contains one element."""
    assert len(mm_dict) == 1


@then("the keyword of this element is the bacteria")
def the_keyword_of_this_element_is_the_bacteria(mm_dict, bacteria_id):
    """the keyword of this element is the bacteria."""
    assert list(mm_dict.keys()) == [bacteria_id]


@then("this element is object of the class Metamodel")
def this_element_is_object_of_the_class_Metamodel(mm_dict, bacteria_id):
    """this element is object of the class Metamodel."""
    assert type(mm_dict[bacteria_id]).__name__ == "Metamodel"
