import os

import numpy.linalg as npl

from dfba_sampling.vizu import plot_res_optim


def assure_valid_directory(dirpath):
    """
    Assure a directory exists, creating if necessary
    Args: dirpath (Str): path of directory
    Returns: nothing
    Exceptions: OSError if directory cannot be created
    """
    if not os.path.isdir(dirpath):
        os.makedirs(dirpath)
    return dirpath


def save_learning_output(mm, path_output):
    """
    method that saves the output of the learning process and the figures related to the optimization process
    - Input :
        * mm: a object from the metamodel class
        * path: the path in which we want to save

    """

    del mm.K
    assure_valid_directory(path_output)
    mm.save_results(path_output / f"metamodel_{mm.bact}_theta.npz")
    mm.load_attributes(theta=path_output / f"metamodel_{mm.bact}_theta.npz")
    reg_v = [
        [
            npl.norm(mm.W[v, :, :].dot(mm.theta[i_compound][:, v]))
            for v in range(len(mm.kept_P[i_compound]))
        ]
        for i_compound in range(mm.Ncompound)
    ]
    Y_hat = mm.flux_estimate(mm.C_learn)
    plot_res_optim(
        mm.P,
        mm.substrate_metabolites,
        mm.compound_name,
        mm.learning_loss,
        reg_v,
        mm.Flux_learn,
        Y_hat,
        path_output,
    )


def gen_dict_metamodel(bacteria_id_list, mm_list, param_list):
    """
    method that creates a dictionary of metamodel, one metamodel per bacteria
    - Input :
        * bacteria_Id_list: list of the name/Id of the bacteria
        * mm_list: list of metamodels each associated to a bacteria
        * param_list: list of path to .npz results files
    - Output :
        * dict_mm: the dictionary containing the name of the bacteria as keyword and the associated metamodel
    """
    dict_mm = {}
    for i in range(len(bacteria_id_list)):
        mm_list[i].load_attributes(theta=param_list[i])
        dict_mm[bacteria_id_list[i]] = mm_list[i]
    return dict_mm
