# -*- coding: utf-8 -*-

from pathlib import Path  # noqa: F401

import matplotlib.pyplot as plt
import numpy as np


def Plot(
    result_y, experiment, file_name="out_BacterialGrowth", axis=None, folder_name=""
):
    """
    Plot ODE solution
    - Input:
        * result_y: np.array with solution
        * experiment: object of the experiment class
        * axis : matplotlib axis, default = None. The axis to plot in.
    - Do:
        * plots and saves in png and eps format
    """

    folder_name = Path(folder_name)

    result = result_y
    if axis is None:
        fig, ax = plt.subplots()
    else:
        ax = axis
    ax2 = ax.twinx()
    for _, b in enumerate(experiment.species):
        ax2.plot(
            result[:, 0],
            result[:, experiment.y_id(b) + 1],
            "-",
            linewidth=2,
            label=b,
        )

    for _, s in enumerate(experiment.substrate_metabolites):
        ax.plot(
            result[:, 0],
            result[:, experiment.y_id(s) + 1],
            "-",
            linewidth=0.5,
            label=s,
        )
    for _, o in enumerate(experiment.output_metabolites):
        ax.plot(
            result[:, 0],
            result[:, experiment.y_id(o) + 1],
            "-",
            linewidth=0.5,
            label=o,
        )
    ax.legend(loc="best", fontsize=12)
    ax2.legend(loc="best", fontsize=12)
    ax.set_xlabel("time (hours)")
    ax.set_ylabel("Concentration (µmol/L)")
    ax2.set_ylabel("Bacterial growth (g/L)")
    fig.suptitle("Bacterial growth.", fontsize=14, fontweight="bold")
    fig.tight_layout()
    plt.savefig(folder_name / f"{file_name}.png", format="png")
    plt.savefig(folder_name / f"{file_name}.pdf", format="pdf")


def PlotFBA(
    result_constraint,
    result_fba,
    experiment,
    file_name="out_FBA",
    axis=None,
    folder_name="",
):
    """
    Plot ODE solution
    - Input:
        * result_constraint: np.array of FBA inputs
        * result_fba: np.array of FBA outputs
        * experiment: object of the experiment class
        * axis : matplotlib axis, default = None. The axis to plot in.
    - Do:
        * plots and saves in png and eps format
    """

    folder_name = Path(folder_name)

    # normalized_constraint=(result_constraint-np.min(result_constraint,axis=0)[None,:])/((np.max(result_constraint,axis=0)-np.min(result_constraint,axis=0)[None,:]))
    normalized_constraint = result_constraint

    if axis is None:
        fig, axs = plt.subplots(
            len(experiment.substrate_metabolites), len(experiment.species)
        )
        ax = {}
        for i in range(len(experiment.substrate_metabolites)):
            ax[i] = {}
            for j in range(len(experiment.species)):
                if len(experiment.species) > 1:
                    ax[i][j] = axs[i, j]
                else:
                    ax[i][j] = axs[i]
    else:
        ax = axis
    for i_s, s in enumerate(experiment.substrate_metabolites):
        for i_b, b in enumerate(experiment.species):
            ax[i_s][i_b].scatter(
                normalized_constraint[:, experiment.y_id(s) + 1],
                result_fba[i_b, :, experiment.y_id(s) + 1],
                s=5,
                label=s,
            )
    for i_b, b in enumerate(experiment.species):
        ax[0][i_b].set_title(b)
        ax[len(experiment.substrate_metabolites) - 1][i_b].set_xlabel("constraint")

    for i_s, s in enumerate(experiment.substrate_metabolites):
        ax[i_s][0].set_ylabel(s + "flux")
    plt.savefig(folder_name / f"{file_name}.png", format="png")
    plt.savefig(folder_name / f"{file_name}.pdf", format="pdf")


def plot_res_optim(
    P, substrate_list, compound_list, residu, reg_v, Y, Y_hat, save_path=None
):
    P_str = [np.array2string(v, separator=",") for v in P]
    txt = ""
    for i, lpi in enumerate(substrate_list):
        txt += str(i) + ": " + lpi + "\n"
    ifig = 0
    plt.figure(ifig, figsize=(10, 5))
    for ic, c in enumerate(compound_list):
        plt.plot(residu[ic], label=c)
    ifig += 1
    plt.yscale("log")
    plt.legend()
    plt.xlabel("iteration")
    plt.ylabel(r"$\frac{||\mathcal{F}-\hat{\mathcal{F}}||_2}{||\mathcal{F}||_2}$")
    if not (save_path is None):
        plt.savefig(save_path / "loss.pdf", format="pdf")
        plt.savefig(save_path / "loss.png", format="png")
        plt.close("all")

    fig, ax = plt.subplots(len(compound_list), 1, sharex=True, figsize=(10, 5))
    for ic, c in enumerate(compound_list):
        reg_v_i = np.array(reg_v[ic])
        P0 = np.array(P_str)[reg_v_i == 0]
        reg_v_0 = reg_v_i[reg_v_i == 0]
        Pn0 = np.array(P_str)[reg_v_i != 0]
        reg_v_n0 = reg_v_i[reg_v_i != 0]
        if not (len(reg_v_n0) == 0):
            ax[ic].stem(Pn0, reg_v_n0, markerfmt="bo")
        if not (len(reg_v_0) == 0):
            ax[ic].stem(P0, reg_v_0, markerfmt="xr")
        ax[ic].annotate(
            c,
            xy=(1.0, 1.0),
            xycoords="axes fraction",
            xytext=(1.0, 1.0),
            textcoords="axes fraction",
            horizontalalignment="right",
            verticalalignment="top",
        )
    ax[ic].annotate(
        txt,
        xy=(1.0, 0.0),
        xycoords="axes fraction",
        xytext=(1.0, 0.0),
        textcoords="axes fraction",
        horizontalalignment="left",
        verticalalignment="top",
    )
    fig.supylabel(r"$ ||W_v\theta_v||_2$")
    fig.supxlabel("$v$")
    fig.tight_layout()
    ifig += 1
    if not (save_path is None):
        plt.savefig(save_path / "groupLASSO_reg.pdf", format="pdf")
        plt.savefig(save_path / "groupLASSO_reg.png", format="png")
        plt.close("all")

    fig2, ax2 = plt.subplots(len(compound_list), 1, sharex=True, figsize=(10, 5))
    for ic, c in enumerate(compound_list):
        ax2[ic].plot(Y[:, ic], "or", label=r"$ \mathcal{F} $")
        ax2[ic].plot(Y_hat[:, ic], "ob", label=r"$ \hat{\mathcal{F} }$")
        if ic == 0:
            ax2[ic].legend(loc="upper left", ncol=2, bbox_to_anchor=(0.0, 1.5))
        ax2[ic].annotate(
            c,
            xy=(1.0, 1.0),
            xycoords="axes fraction",
            xytext=(1.0, 1.0),
            textcoords="axes fraction",
            horizontalalignment="right",
            verticalalignment="top",
        )

    fig2.supylabel(r"flux")
    fig2.tight_layout()
    if not (save_path is None):
        plt.savefig(save_path / "flux_comp_est_set.pdf", format="pdf")
        plt.savefig(save_path / "flux_comp_est_set.png", format="png")
        plt.close("all")
    ifig += 1

    fig3, ax3 = plt.subplots(len(compound_list), 1, figsize=(10, 5))
    for ic, c in enumerate(compound_list):
        ax3[ic].plot(Y[:, ic], Y_hat[:, ic], "bo")
        ax3[ic].annotate(
            c,
            xy=(1.0, 1.0),
            xycoords="axes fraction",
            xytext=(1.0, 1.0),
            textcoords="axes fraction",
            horizontalalignment="right",
            verticalalignment="top",
        )
        # plt.title(c)
    fig3.supylabel(r"$ \hat{\mathcal{F}} $")
    fig3.supxlabel(r"$ \mathcal{F}$")
    fig3.tight_layout()
    ifig += 1
    if not (save_path is None):
        plt.savefig(save_path / "flux_est_vs_set.pdf", format="pdf")
        plt.savefig(save_path / "flux_est_vs_set.png", format="png")
        plt.close("all")
    else:
        plt.show()
