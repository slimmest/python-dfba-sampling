import argparse

from dfba_sampling.constraint_sampler import Constraint_Sampler

parser = argparse.ArgumentParser(description="Command description.")
parser.add_argument(
    "-e",
    "--exp_config",
    help="dFBA parameters, initial conditions, exchange reactions (YAML)",
    type=str,
)
parser.add_argument(
    "-s",
    "--sample_config",
    help="sampling parameters, output paths (YAML)",
    type=str,
)


def main(args=None):
    args = parser.parse_args(args=args)
    cs = Constraint_Sampler(
        exp_config=args.exp_config, sample_config=args.sample_config
    )
    cs.sample_yaml_files()
    # cs.assemble()


main()
