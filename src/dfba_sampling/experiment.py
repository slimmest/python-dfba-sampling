# -*- coding: utf-8 -*-

import os
from pathlib import Path  # noqa: F401

import cobra
import numpy as np
import pandas as pd
import yaml


class ExperimentSpec(object):
    def __init__(self, species=None, param=None, config=None, networks=None):
        """init."""
        # for a bacterium, store metabolites (substrates and outputs) and associated reactions
        if config is not None:
            with open(config, "r") as stream:
                param = yaml.safe_load(stream)
            self.config = config  # for saving purposes.

        self.species = []
        self._species = {}
        self._substrate_metabolites = {}
        self._output_metabolites = {}
        self._substrate_reaction = {}
        self._output_reaction = {}
        self._biomass_reaction = {}
        self._y0 = {}
        self._sbml_model = {}
        self._order_reactions = {}

        if isinstance(param, dict):
            self.add_substrate_metabolites(param["Substrate_metabolites"])
            self.add_output_metabolites(param["Output_metabolites"])
            self.add_species(param["species"])

        self.networks = networks if networks is not None else []
        self.input_networks()

        if param is not None:
            self.time = param["Time"]
            self.time_step = param["Timestep"]
            self.limit_dt = param["limit_dt"]
            self.nb_timestep = int(self.time / self.time_step) + 1

            self.compound_name = (
                self.substrate_metabolites + self.output_metabolites + self.species
            )
            self.intrinsic_flux = param["intrinsic_flux"] * np.ones(
                len(self.compound_name)
            )

    def __eq__(self, other):
        """
        Method allowing to compare two objects of the same class.
        Object comparison is done through comparison of two specific
        attributes.
        Input:
            * other: assumed to be an object of the Experiment class.
        """
        if isinstance(self, other.__class__):
            return set(self.output_metabolites) == set(
                other.output_metabolites
            ) and set(self.substrate_metabolites) == set(other.substrate_metabolites)

    def add_species(self, species={}):
        """
        A method to add species and associated reactions.
        Input:
            * species: dict, similar as a species entry in YAML file.
        """
        for id, param in species.items():
            self._substrate_reaction[id] = param["Substrate_Reaction_Map"]
            self._output_reaction[id] = param["Output_Reaction_Map"]
            self._biomass_reaction[id] = param["biomass_name"]
            self._y0[id] = float(param["initial_value"])
            self._sbml_model[id] = param["sbml_model"]
            self._species[id] = param
            if "Reaction_order_for_lexicographic_FBA_resolution" in param.keys():
                self._order_reactions[id] = param[
                    "Reaction_order_for_lexicographic_FBA_resolution"
                ]
            else:
                self._order_reactions[id] = None
        self._update()

    def add_substrate_metabolites(self, met={}):
        """
        Method to add a substrate metabolite to the Experiment.
        - Input:
            * met: dict with keys metabolites names value initial_value.
        """
        for id, param in met.items():
            self._substrate_metabolites[id] = param
            self._y0[id] = float(param["initial_value"])
        self._update()

    def add_output_metabolites(self, met={}):
        """
        Method to add an output metabolite to the Experiment.
        - Input:
            * met: dict with keys metabolites names value initial_value.
        """
        for id, param in met.items():
            self._output_metabolites[id] = param
            self._y0[id] = float(param["initial_value"])
        self._update()

    def add_output_reaction(self, species={}):
        """
        Method to add an output reaction, assuming output metabolite
        has already been specified.
        Input:
            * species: dict of the form {bact: {"output":"reac_output"}}
        """
        for id, param in species.items():
            self._output_reaction[id] = param

    def add_substrate_reaction(self, species={}):
        """
        Method to add a substrate reaction, assuming output metabolite
        has already been specified.
        Input:
            * species: dict of the form {bact: {"substrate":"reac_substrate"}}
        """
        for id, param in species.items():
            self._substrate_reaction[id] = param

    def add_biomass_reaction(self, species={}):
        """
        Method to add a biomass reaction, assuming corresponding bacterial
        species has already been specified.
        Input:
            * species: dict of the form {"bact":"reac_biomass"}
        """
        for id, param in species.items():
            self._biomass_reaction[id] = species[id]
        self._update()

    def _update(self):
        """Method to update attributes once compounds have been added."""
        self.species = list(self._species.keys())
        self.substrate_metabolites = list(self._substrate_metabolites.keys())
        self.output_metabolites = list(self._output_metabolites.keys())
        self.y0 = np.array(list(self._y0.values()))
        self.biomass = list(self._biomass_reaction.keys())

    def output_reaction(self, species):
        """
        Method to obtain the output reactions for a given bacterial species.
        - Input:
            * species: str, bacterial species.
        - Output:
            * dict with keys output_metabolites and associated reactions.
        """
        return self._output_reaction[species]

    def substrate_reaction(self, species):
        """Method to obtain the substrate metabolites of a bacterial species.
        - Input:
            * species: str, bacterial species.
        - Output:
            * list of substrate metabolites.
        """
        return self._substrate_reaction[species]

    def substrate_species(self, met):
        """
        Method to obtain the bacterial species target by a metabolite.
        - Input:
            * met: str, substrate metabolite.
        - Output:
            * list of bacterial species.
        """
        return [s for s in self.species if met in self._substrate_reaction[s].keys()]

    def y_id(self, compound):
        """
        Method
        """

        return self.compound_name.index(compound)

    def observables(self, species):
        """
        Method to obtain the observables for a given species.
        - Input:
            * species: str, bacterial species.
        - Output:
            * reac_list: list of tuple (compound, reaction)
        """
        reac_list = [
            (met, reaction)
            for met, reaction in self._substrate_reaction[species].items()
        ]

        reac_list += [
            (met, reaction) for met, reaction in self._output_reaction[species].items()
        ]

        reac_list += [(species, self._biomass_reaction[species])]

        return reac_list

    def input_networks(self):
        """
        Method that will create the attribute model with
        the cobra SBML model stored.
        """

        # Collect network filenames specified in YAML files.
        yaml_networks = [
            self._sbml_model[s] for s in self.species if hasattr(self, "_sbml_model")
        ]
        # Read SBML models specified in YAML files or explicitly in networks.
        self.models = {
            model.id: model
            for model in [
                cobra.io.read_sbml_model(n)
                for n in yaml_networks + self.networks
                if os.path.isfile(n)
            ]
        }

    def save(self, outfile):
        """
        Method to save the current experiment specification in a YAML file
        stored in the output directory.
        """

        outparam = {}

        outparam["Time"] = self.time
        outparam["Timestep"] = self.time_step
        outparam["limit_dt"] = self.limit_dt
        # intrinsic flux are all equal so far
        outparam["intrinsic_flux"] = float(self.intrinsic_flux[0])

        outparam["Output_metabolites"] = {
            i: {
                "initial_value": np.format_float_scientific(
                    self.y0[self.y_id(i)], exp_digits=1, trim="-"
                )
            }
            for i in self.output_metabolites
        }

        outparam["Substrate_metabolites"] = {
            i: {
                "initial_value": np.format_float_scientific(
                    self.y0[self.y_id(i)], exp_digits=1, trim="-"
                )
            }
            for i in self.substrate_metabolites
        }

        outparam["species"] = {
            s: {
                "Output_Reaction_Map": self.output_reaction(s),
                "Substrate_Reaction_Map": self.substrate_reaction(s),
                "biomass_name": self._biomass_reaction[s],
                "initial_value": np.format_float_scientific(
                    self.y0[self.y_id(s)], exp_digits=1, trim="-"
                ),
                "sbml_model": self._sbml_model[s],
                "Reaction_order_for_lexicographic_FBA_resolution": self._order_reactions[
                    s
                ],
            }
            for s in self.species
        }

        with open(outfile, "w") as f:
            yaml.dump(outparam, f, default_flow_style=False, sort_keys=False)


class ExperimentResult(object):
    def __init__(self, experiment, y, constraints, fba):
        """init."""
        self.y = y
        self.constraints = constraints
        self.fba = fba
        self.experiment = experiment

    def save(self, outdir):
        """
        Method to save the results of TimeIntegration in .tsv files.
        """

        outdir = Path(outdir)

        l1_dfba = ["t"]
        l1_constraint = ["t"]

        msg_compound = "# the compounds are: "
        for c in self.experiment.compound_name:
            msg_compound += c + ", "
            l1_dfba.append(c)
        msg_compound += " \n"
        for sm in self.experiment.substrate_metabolites:
            l1_constraint.append(sm)
        msg_fluxnom = "# the nominal flux is: "
        for f in self.experiment.intrinsic_flux:
            msg_fluxnom += str(f) + ", "
        msg_fluxnom += " \n"
        msg_initconc = "# the initial concentration are: "
        for c0 in self.experiment.y0:
            msg_initconc += str(c0) + ", "
        msg_initconc += " \n"
        h_dfba = "# results of the dFBA model \n# the columns are: t and the concentrations in each compounds\n"
        h_dfba += msg_compound + msg_fluxnom + msg_initconc
        df = pd.DataFrame(self.y, columns=l1_dfba)
        with open(outdir / "out_dFBA.tsv", "w") as w:
            w.write(h_dfba)
            df.to_csv(w, header=True, index=False)
        for j, f in enumerate(self.experiment.species):
            h_fba = "# results of the FBA model for the bactery " + f
            h_fba += "\n# the columns are: t and the metabolic fluxes \n"
            h_fba += msg_compound + msg_fluxnom + msg_initconc
            df = pd.DataFrame(self.fba[j], columns=l1_dfba)
            with open(outdir / f"out_FBA_{f}.tsv", "w") as w:
                w.write(h_fba + "\n")
                df.to_csv(w, header=True, index=False)
        h_constraint = "# uptake constraint \n# the columns are: t and the constraint for each substrate metabolites\n"
        h_constraint += msg_compound + msg_fluxnom + msg_initconc

        df = pd.DataFrame(self.constraints, columns=l1_constraint)
        with open(outdir / "out_constraint.tsv", "w") as w:
            w.write(h_constraint + "\n")
            df.to_csv(w, header=True, index=False)
