# -*- coding: utf-8 -*-
import cobra as cp
import numpy as np


class Mock_sol:
    """
        This empty class is introduced to encapsulate solutions obtained
    with the function cobra.util.add_lexicographic_constraints, which
    returns a pandas serie, with equivalent fields of the class FBA results
    returned by the function model.optimize()
    """

    def __init__(self, experiment, bacteria_id):
        self.fluxes = {
            reaction: np.nan for met, reaction in experiment.observables(bacteria_id)
        }
        self.status = None
        pass


def FBA_model_Wrapper(constraint, bacteria_id, experiment, Total_FLUX=False):
    """
    This function maps constraints to the corresponding FBA fluxes for
    the bacteria and metabolites screened in the dFBA
    - Input:
        * constraint: dictionary
        * bacteria_id : string
        * experiment: object of the Experiment class
    - Output:
        * F: numpy array of fluxes (F.shape = (len(param['compound_name']),))
        * flag_err : boolean. Indicates if FBA failed (infeasible status)
        * total_flux: dictionary of all reaction fluxes
    """
    pfba_sol = Mock_sol(experiment, bacteria_id)
    optimal_flux = np.zeros((constraint.shape[0], len(experiment.compound_name)))
    if Total_FLUX:
        total_flux = {}  # New dictionary to store all reaction fluxes
    flag_negative_constraint = True
    flag_err = ""

    for i_constraint in range(constraint.shape[0]):
        with experiment.models[bacteria_id] as mod:
            for (
                met
            ) in (
                experiment.substrate_metabolites
            ):  # for each limiting substrate, we define the new lower bound
                if met in experiment.substrate_reaction(bacteria_id):
                    reac_mod = mod.reactions.get_by_id(
                        experiment.substrate_reaction(bacteria_id)[met]
                    )
                    try:
                        reac_mod.lower_bound = constraint[
                            i_constraint, experiment.y_id(met)
                        ]
                    except Exception:
                        print("Error in constraint access")
                        pfba_sol.fluxes = {k: 0.0 for k in pfba_sol.fluxes.keys()}
                        pfba_sol.status = "infeasible"
                        flag_negative_constraint = False

            if (
                experiment._order_reactions[bacteria_id] is not None
                and flag_negative_constraint
            ):
                try:
                    cp.util.fix_objective_as_constraint(mod)
                    pfba_sol.fluxes = cp.util.add_lexicographic_constraints(
                        mod,
                        experiment._order_reactions[bacteria_id],
                        ["max" for _ in experiment._order_reactions[bacteria_id]],
                    )
                    pfba_sol.status = "optimal"
                except Exception:
                    print("Warning: lexicographic computation failed")
                    pfba_sol.fluxes = {k: 0.0 for k in pfba_sol.fluxes.keys()}
                    pfba_sol.status = "infeasible"
            else:
                pfba_sol = mod.optimize()

        flag_err = pfba_sol.status

        if flag_err == "optimal":
            for met, reaction in experiment.observables(bacteria_id):
                optimal_flux[i_constraint, experiment.y_id(met)] = pfba_sol.fluxes[
                    reaction
                ]

            if Total_FLUX:
                # Store all reaction fluxes in a dictionary
                total_flux = {
                    reaction: flux for reaction, flux in pfba_sol.fluxes.items()
                }

        else:
            # optimal_flux = np.zeros((len(experiment.compound_name),))
            flag_err = f"Constraint {constraint[i_constraint]} led to model infeasible in {bacteria_id}"
            print(flag_err)

    if constraint.shape[0] == 1:
        optimal_flux = optimal_flux.reshape((len(experiment.compound_name),))

    if Total_FLUX:
        return optimal_flux, flag_err, total_flux
    else:
        return optimal_flux, flag_err
