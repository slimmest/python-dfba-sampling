# -*- coding: utf-8 -*-
import numpy as np


class UniformX(object):
    def __init__(self, C, bin=100):
        """
        Instanciation of the class.
        - input :
            * C : np.array of constraint samples
            * bin : number of bins in (0,1)
        """
        self.C = C
        self.bin = bin
        self.quantiles = np.linspace(0, 1, bin)

        self.thresholds = np.quantile(C, self.quantiles, axis=0)

    def InUnifSpace(self, X):
        """
        For a given sample X of n points in the constraint parameter space,
        returns the corresponding n points in the uniform space
        - input:
            * X: numpy array of dimension n*dim_constraint_space
        - output:
            *UnifX: numpy array of dimension n*dim_constraint_space
        """

        UnifX = np.zeros(X.shape)
        for i in np.arange(X.shape[1]):
            UnifX[:, i] = self.quantiles[
                np.searchsorted(self.thresholds[:, i], X[:, i])
            ]
        return UnifX
