#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  9 15:43:22 2021
Corrected on Jan. 21 2025

@author: malou, Labarthe
"""
import itertools as it

import numpy as np
import pandas as pd
import yaml
from scipy.linalg import sqrtm
from sklearn.metrics.pairwise import pairwise_kernels
from sklearn.preprocessing import MinMaxScaler

from dfba_sampling.optimization import error_fct
from dfba_sampling.optimization import solve_groupLASSO


class LogitTransformer:
    def __init__(
        self,
        epsilon=1.0e-12,
        MinMaxScaling=True,
    ):
        self.epsilon = epsilon
        self.MinMaxScaling = MinMaxScaling
        self.MinMaxScaler = None

    def fit(
        self,
        X,
    ):
        if self.MinMaxScaling:
            self.MinMaxScaler = MinMaxScaler()
            self.MinMaxScaler.fit(X)

    def fit_transform(
        self,
        X,
    ):
        if self.MinMaxScaling:
            self.MinMaxScaler = MinMaxScaler()
            curr_X = self.MinMaxScaler.fit_transform(X)
        else:
            curr_X = X
        curr_X = curr_X.clip(self.epsilon, 1.0 - self.epsilon)
        return np.log((curr_X + self.epsilon) / (1.0 - (curr_X - self.epsilon)))

    def transform(
        self,
        X,
    ):
        if self.MinMaxScaling:
            curr_X = self.MinMaxScaler.transform(X)
        else:
            curr_X = X
        curr_X = curr_X.clip(self.epsilon, 1.0 - self.epsilon)
        return np.log((curr_X + self.epsilon) / (1.0 - (curr_X - self.epsilon)))

    def inverse_transform(
        self,
        X,
    ):
        Y = (1 - self.epsilon * (np.exp(-X) + 1)) / (1 + np.exp(-X))
        if self.MinMaxScaling:
            Y = self.MinMaxScaler.inverse_transform(Y)
        return Y


class Metamodel(object):
    def __init__(
        self,
        constraints=None,
        fluxes=None,
        order_interaction_max=None,
        bact="",
        kernel_type="Matern",
        index_col=None,
        exp=None,
        metamodel_config=None,
        relative_path='./',
    ):
        self.kernel_type = kernel_type
        self.bact = bact
        self.order_interaction_max = order_interaction_max
        self.exp = exp
        self.kept_kv = None
        self.kept_P = None
        self.K_sqrt = None
        self.Scaler_constraint = None
        self.Scaler_fluxes = None
        self.Scaler_constraint_flag = "None"
        self.Scaler_fluxes_flag = "None"
        self.Clipping = None
        if constraints is not None:
            self.new_constraintlearningset(constraints, index_col=index_col)
            self.Nobs, self.Nsub = np.shape(self.C_learn)
        if fluxes is not None:
            self.new_fluxlearningset(fluxes, index_col=index_col)
        if metamodel_config is not None:
            with open(metamodel_config, "r") as file:  # Load the user parameter yml
                self.metamodel_yml = yaml.safe_load(file)
            if (
                "selected_hyperparameters" in self.metamodel_yml.keys()
                and "identity" in self.metamodel_yml["selected_hyperparameters"]
            ):
                tmp_path=relative_path+self.metamodel_yml["selected_hyperparameters"]["identity"]
                with open(
                    tmp_path, "r"
                ) as file:  # Load the user parameter yml
                    identity = yaml.safe_load(file)
                    for spec in identity:
                        self.metamodel_yml[spec]["identity"] = identity[spec]
        else:
            self.metamodel_yml = None

    def new_constraintlearningset(
        self,
        constraints,
        n_obs=None,
        index_col=None,
        X_Scaling="None",
    ):
        """
        method that stores a given arrays in self.C_learn and the list of substrates
        - Input :
            * self: a object from the metamodel class
            * constraints: path to constraint learning set tsv file or panda dataframe
            * n_obs: number of observations
            * index_col: column index in the csv file if provided
            * X_Scaling: str, either 'None' (no scaling), 'MinMax' (MinMax scaling) or 'Logit' (Logit scaling)
        """
        if isinstance(constraints, str):
            self.C_learn = pd.read_csv(constraints, sep="\t", index_col=index_col)
        else:
            self.C_learn = constraints
        if self.exp is not None:
            col_subset = [
                c
                for c in self.C_learn.columns
                if c in self.exp._substrate_reaction[self.bact].keys()
            ]
            self.C_learn = self.C_learn[col_subset]
            self.constraint_ID_in_Y = np.array([self.exp.y_id(i) for i in col_subset])
            if (self.metamodel_yml is not None
                and "selected_hyperparameters" in self.metamodel_yml.keys()
                and "identity" in self.metamodel_yml["selected_hyperparameters"]
            ):
                self.constraint_Id_in_Y_identity = [
                        self.exp.y_id(comp)
                        for comp in self.metamodel_yml[self.bact]["identity"]
                    ]                
        else:
            self.constraint_ID_in_Y = np.arange(self.C_learn.shape[1])
        self.substrate_metabolites = list(self.C_learn.columns)
        self.C_learn = self.C_learn.values[:n_obs]

        if X_Scaling not in ["None", "MinMax", "Logit"]:
            raise NameError(
                f"You gave {X_Scaling} as scaler, but only '' (no scaling), 'MinMax' (MinMax scaler) or 'Logit' (Logit scaler) are allowed."
            )
        self.Scaler_constraint_flag = X_Scaling
        if X_Scaling == "MinMax":
            self.Scaler_constraint = MinMaxScaler()
            self.C_learn = self.Scaler_constraint.fit_transform(self.C_learn)
        elif X_Scaling == "Logit":
            self.Scaler_constraint = LogitTransformer()
            # if not MinMaxScaling:
            #     self.MinMaxScaler_constraint = self.LogitTransformer.MinMaxScaler
            self.C_learn = self.Scaler_constraint.fit_transform(self.C_learn)

        self.Nobs, self.Nsub = np.shape(self.C_learn)
        self.esp_ka_joint_list = [
            self.esp_ka_joint(self.C_learn[:, a]) for a in range(self.Nsub)
        ]
        self.esp_ka_marg_mat = np.array(
            [
                [
                    self.esp_ka_marg(self.C_learn[i, a], self.C_learn[:, a])
                    for a in range(self.Nsub)
                ]
                for i in range(self.Nobs)
            ]
        )

    def new_fluxlearningset(
        self,
        fluxes,
        n_obs=None,
        index_col=None,
        Y_Scaling="None",
    ):
        """
        method that stores a given arrays in self.Flux_learn
        - Input :
            * self: a object from the metamodel class
            * fluxes: path to flux learning set tsv file or panda dataframe
            * n_obs: number of observations
            * index_col: column index in the csv file if provided
        """
        if Y_Scaling not in ["None", "MinMax", "Logit"]:
            raise NameError(
                f"You gave {Y_Scaling} as scaler, but only 'None' (no scaling),\
                      'MinMax' (MinMax scaler) or 'Logit' (Logit scaler) are alowed."
            )
        if isinstance(fluxes, str):
            self.Flux_learn = pd.read_csv(fluxes, sep="\t", index_col=index_col)
        else:
            self.Flux_learn = fluxes
        if self.exp is not None:
            col_subset = [
                c
                for c in self.Flux_learn.columns
                if c
                in list(self.exp._substrate_reaction[self.bact].keys())
                + list(self.exp._output_reaction[self.bact].keys())
                + [self.bact]
            ]
            self.Flux_learn = self.Flux_learn[col_subset]
            self.Flux_ID_in_Y = np.array([self.exp.y_id(i) for i in col_subset])
            if (self.metamodel_yml is not None
                and "selected_hyperparameters" in self.metamodel_yml.keys()
                and "identity" in self.metamodel_yml["selected_hyperparameters"]
            ):
                self.Flux_Id_in_output_identity = [
                        col_subset.index(comp)
                        for comp in self.metamodel_yml[self.bact]["identity"]
                    ]
        else:
            self.Flux_ID_in_Y = np.arange(self.Flux_learn.shape[1])
        self.compound_name = list(self.Flux_learn.columns)
        self.Clipping = MinMaxScaler()
        self.Clipping.fit(self.Flux_learn)
        self.Flux_learn = self.Flux_learn.values[:n_obs]
        self.Scaler_fluxes_flag = Y_Scaling
        if Y_Scaling == "MinMax":
            self.Scaler_fluxes = MinMaxScaler()
            self.Flux_learn = self.Scaler_fluxes.fit_transform(self.Flux_learn)
        elif Y_Scaling == "Logit":
            self.Scaler_fluxes = LogitTransformer()
            self.Flux_learn = self.Scaler_fluxes.fit_transform(self.Flux_learn)
        # Learn the min and max of the dataset for future clipping
        self.Ncompound = self.Flux_learn.shape[1]
        self.learning_loss = [None] * self.Ncompound

    def load_attributes(self, gram_matrix=None, theta=None):
        """
        method to load already computed gram matrix and parameters from files.
        - Intput:
            * gram_matrix: path to gram matrix npz file
            * theta: path to theta parameters npz file
        """
        if gram_matrix is not None:
            self.K = np.load(gram_matrix)["K"]
        if theta is not None:
            try:  # theta is a file path or a string
                params = np.load(theta)
                self.theta, self.theta_0 = params["theta"], params["theta_0"]
            except TypeError:  # theta is a dict
                params = np.load(theta[list(theta.keys())[0]])
                # Get theta support
                self.theta, self.theta_0 = params["theta"], params["theta_0"]
                for k in theta.keys():
                    comp = self.compound_name.index(k)
                    params = np.load(theta[k])
                    curr_theta, curr_theta_0 = params["theta"], params["theta_0"]
                    self.theta[:, :, comp] = curr_theta[:, :, comp]
                    self.theta_0[comp] = curr_theta_0[comp]
            if self.theta.shape[-1] != self.Ncompound:
                raise NameError(
                    "Theta and learning flux shapes are not consistent."
                    + "Thetas have been learnt with an old version of the package."
                )
            self.kept_kv = np.logical_not((self.theta == 0.0).all(axis=0))
            self.kept_P = {
                i: np.array(self.P, dtype=object)[self.kept_kv[:, i]]
                for i in range(self.Ncompound)
            }
            theta = {
                i: self.theta[:, self.kept_kv[:, i], i] for i in range(self.Ncompound)
            }
            self.theta = theta

    def P_OneOrder(self, order_interaction):
        """
        method that generates a list of indexes forming the constraint subsets for a given order of interaction
        - Input :
            * self: a object from the metamodel class
            * order_interaction: the order of interaction of interest
        - Output:
            * : list of indices forming the subsets

        """
        return list(it.combinations(range(self.Nsub), order_interaction))

    def P_SeveralOrder(self):
        """
        method that generates a list of indexes forming the constraint subsets for an array of orders of interaction
        the array of orders of interaction is contained in self.order_interaction_array
        store this list of indices in self.P which denotes the set of subsets
        - Input :
            * self: a object from the metamodel class

        """
        P = []
        for i in self.order_interaction_array:
            P += self.P_OneOrder(i + 1)
        self.P = [np.array(v) for v in P]
        self.Nv = len(self.P)

    def P_MaxOrder(self, max_order_interaction):
        """
        method that generates a list of indices forming the constraint subsets for a maximal order of interaction
        update the array of orders of interaction in consequence and the set of subsets
        - Input :
            * self: a object from the metamodel class
            * max_order_interaction: the maximal order of interaction considered

        """
        self.order_interaction_array = range(max_order_interaction)
        self.P_SeveralOrder()
        self.order_interaction_max = max_order_interaction

    def ka(self, c1, c2):
        """
        method that computes the substrate-wise kernel
        k_a : R x R -> R
            : c1,c2 -> k_a(c1,c2)
        for now, only the Matern kernel is implemented
        - Input :
            * self: a object from the metamodel class
            * c1, c2: two values of constraint for a given substrate and for a given bacteria
        - Output :
            * : the value of the kernel for these constraints
        """
        if self.kernel_type == "Matern":
            k = (1 + 2 * np.abs(c1 - c2)) * np.exp(-2 * np.abs(c1 - c2))
        elif self.kernel_type == "Gaussian":
            k = np.exp(-0.5 * (c1 - c2) ** 2) / np.sqrt(2 * np.pi)
        else:
            raise NameError("Specified kernel type is unknown")
        return k

    def ka_predict(self, c1, c2):
        """
        method that computes the substrate-wise kernel
        k_a : R x R -> R
            : c1,c2 -> k_a(c1,c2)
        for now, only the Matern kernel is implemented
        - Input :
            * self: a object from the metamodel class
            * c1, c2: two vectors respectively of dimension nobs and npred
            (number of points in the batch) of constraint for a given substrate and for a given bacteria
        - Output :
            * : the value of the kernel for these constraints

        """
        if self.kernel_type == "Matern":
            k = (1 + 2 * np.abs(c1[:, None] - c2[None, :])) * np.exp(
                -2 * np.abs(c1[:, None] - c2[None, :])
            )
        elif self.kernel_type == "Gaussian":
            k = np.exp(-0.5 * (c1[:, None] - c2[None, :]) ** 2) / np.sqrt(2 * np.pi)
        else:
            raise NameError("Specified kernel type is unknown")
        return k

    def esp_ka_marg(self, c, clearn_a):
        """
        method that estimates the esperance E[ka(c,U)] with U follows the marginal law: U~Pa
        - Input :
            * self: a object from the metamodel class
            * c: a float, value of constraint for a given compound and for a given bacteria
            * cobs_a: the sampling of the probability law Pa used in the learning set
        - Output :
            * : the estimation of  E[ka(c,U~Pa)]

        """
        return np.mean(self.ka(c, clearn_a))

    def esp_ka_marg_pred(self, ka):
        """
        method that estimates the esperance E[ka(c,U)] with U follows the marginal law: U~Pa
        - Input :
            * self: a object from the metamodel class
            * ka : matrix of dimension nobs * npts
        - Output :
            * : the estimation of  E[ka(c,U~Pa)]

        """
        return np.mean(ka, axis=1)

    def esp_ka_joint(self, cobs_a):
        """
        method that estimates the esperance E[ka(U,V)] with (U,V) follows the joint law: (U,V)~Pa x Pa
        - Input :
            * self: a object from the metamodel class
            * cobs_a: the sampling of the probability law Pa used in the learning set
        - Output :
            * : the estimation of  E[ka(U~Pa,V~Pa)]

        """
        return np.mean(np.array([self.esp_ka_marg(c, cobs_a) for c in cobs_a]))

    def ka0(self, c1, c2, a):
        """
        method that computes from two vectors of constraint c1 and c2 the substrate-wise kernel with mean 0
        k_a0 : R   x R     -> R
             : c1_a,c2_a -> k_a(c1_a,c2_a) - E[ka(c1_a,U~Pa)] x E[ka(c2_a,U~Pa)] / E[ka(U~Pa,V~Pa)]
        - Input :
            * self: a object from the metamodel class
            * c1, c2: two vector of size Nsub containing the constraints for a given bacteria
            * a: the index of the substrate of interest
        - Output :
            * : the value of the kernel with mean 0 for the constraints of the substrate of interest

        """
        ka = self.ka(c1[a], c2[a])

        zero_mean = (
            self.esp_ka_marg(c1[a], self.C_learn[:, a])
            * self.esp_ka_marg(c2[a], self.C_learn[:, a])
            / self.esp_ka_joint_list[a]
        )
        return ka - zero_mean

    def ka0_predict(self, c, a):
        """
        method similar to ka0, using a precomputed average
        (only the average with E(c, U) is computed, with c the
        new constraint point) and for a batch input c
        inputs:
            - c: a matrix of constraints of dimension (Npred, Nsub)
            (number of constraints in the batch to predict from, number of considered fluxes)
            - a : part of P
        outputs:
            - ka0 : numpy array of dimension Npts x Nobs
        """

        ka = self.ka_predict(c[:, a], self.C_learn[:, a])
        zero_mean = (
            self.esp_ka_marg_mat[None, :, a]
            * self.esp_ka_marg_pred(ka)[:, None]
            / self.esp_ka_joint_list[a]
        )
        return ka - zero_mean

    def predict(self, c):
        """
        interface method to mimic scikit-learn syntax
        - Input:
            * self: a object from the metamodel class
            * c: a matrix of constraints of dimension (Npred, Ny)
             (number of constraints in the batch to predict from, number of considered fluxes)
        - Output :
            * : the estimate of the flux computed by the metamodel
        """
        return self.flux_estimate(c)

    def flux_estimate(self, c_in):
        """
        method that estimates the flux for new constraints during the online phase using the metamodel
        - Input :
            * self: a object from the metamodel class
            * c: a matrix of constraints of dimension (Npred, Ny)
             (number of constraints in the batch to predict from, number of considered fluxes)
        - Output :
            * : the estimate of the flux computed by the metamodel

        """
        Npred = c_in.shape[0]
        if self.exp is not None:
            output_y0 = np.zeros([Npred, len(self.exp.compound_name)])
        else:
            output_y0 = np.zeros([Npred, self.Ncompound])

        c = c_in[:, self.constraint_ID_in_Y]
        if c.shape[0] > 0:
            if self.Scaler_constraint_flag != "None":
                c = self.Scaler_constraint.transform(c)
            kernel_singleton_OrderOne = np.zeros([Npred, self.Nobs, self.Nsub])
            for s in range(self.Nsub):
                kernel_singleton_OrderOne[:, :, s] = self.ka0_predict(c, s)
            output = np.zeros([Npred, self.Ncompound])
            for comp in range(self.Ncompound):
                if (
                    self.metamodel_yml is None
                    or "identity" not in self.metamodel_yml[self.bact]
                    or self.compound_name[comp]
                    not in self.metamodel_yml[self.bact]["identity"]
                ):
                    kernel_singleton = np.zeros(
                        [Npred, self.Nobs, len(self.kept_P[comp])]
                    )
                    for a in range(len(self.kept_P[comp])):
                        if len(self.kept_P[comp][a]) == 1:
                            kernel_singleton[:, :, a] = kernel_singleton_OrderOne[
                                :, :, self.kept_P[comp][a][0]
                            ]
                        else:
                            kernel_singleton[:, :, a] = np.prod(
                                kernel_singleton_OrderOne[:, :, self.kept_P[comp][a]],
                                axis=2,
                            )
                    output[:, comp] = self.theta_0[comp] + np.sum(
                        [
                            kernel_singleton[:, :, iv].dot(self.theta[comp][:, iv])
                            for iv in range(len(self.kept_P[comp]))
                        ],
                        axis=0,
                    )
            if self.Scaler_fluxes_flag != "None":
                output = self.Scaler_fluxes.inverse_transform(output)
            if (self.metamodel_yml is not None
                and "selected_hyperparameters" in self.metamodel_yml.keys()
                and "identity" in self.metamodel_yml["selected_hyperparameters"]
            ):
                output[:,self.Flux_Id_in_output_identity] = c_in[:,self.constraint_Id_in_Y_identity]                                     
            output = output.clip(
                self.Clipping.data_min_,
                self.Clipping.data_max_,
            )
        else:
            output = 0.0
        output_y0[:, self.Flux_ID_in_Y] = output        
        return output_y0

    def Gram_matrix_sqrt(self, eps=1e-8):
        """Register in array self.K_sqrt the square root of  Gram matrices for all substets v of the constraints of the learning set
        - Input:
            * self: metamodel class
            * eps: a regulation number used to shift small eigen values towards positive values.
            Theoretically, the K matrix are definite positive. But practically rounding errors can lead
            to imaginary or negative eigen values => adding eps*identity for regularisation
        """
        self.K_sqrt = np.zeros([len(self.P), self.Nobs, self.Nobs])
        for i, v in enumerate(self.P):
            print("Generation of the sqrt of the Gram matrix for the subset ", v)
            reg_eps = eps * np.eye(self.Nobs)
            self.K_sqrt[i] = sqrtm(self.K[i] + reg_eps)

    def GramMatrix(self):
        """
        method that stores in the 3D array self.K the Gram matrices for all the subsets v of the constraints of the learning set
        - Input :
            * self: a object from the metamodel class
        """

        if self.order_interaction_max is None:
            self.P_MaxOrder(self.Nsub)
        else:
            self.P_MaxOrder(self.order_interaction_max)

        self.K = np.zeros([len(self.P), self.Nobs, self.Nobs])
        # requires P to have been generated in increasing size order
        for i, v in enumerate(self.P):
            print("Generation of the Gram matrix for the subset ", v)
            if len(v) == 1:
                self.K[i] = pairwise_kernels(
                    self.C_learn, n_jobs=1, metric=lambda c1, c2: self.ka0(c1, c2, i)
                )
            else:
                self.K[i] = np.prod(self.K[v], axis=0)

    def loss_fct(self, F_test, C_test):
        F_hat = np.array(
            [
                self.flux_estimate(C_test[itest, :])
                for itest in range(np.shape(C_test)[0])
            ]
        )
        if F_test.shape[1] != F_hat.shape[1]:
            raise NameError(
                "F_test.shape is "
                + F_test.shape
                + " whereas F_hat.shape is "
                + F_hat.shape
                + ". You should use the function"
                + " with a subset of F_test columns."
            )
        return [error_fct(F_test[:, i], F_hat[:, i]) for i in range(self.Ncompound)]
        # return Parallel(n_jobs=self.Ncompound)(
        #    delayed(error_fct)(F_test[:, i], F_hat[:, i]) for i in range(self.Ncompound)
        # )

    def gen_Wv(self, type_weight="identity", comp=None, eps_sqrt=1e-8):
        """
        method that generatew the weight matrices of the LASSO group regularization metric
        for now, only the identity weight matrices are implemented
        - Input :
            * self: a object from the metamodel class
            * type_weight: the type of weight that is going to be used
        - Output :
            * : the estimate of the flux computed by the metamodel

        """
        if type_weight == "identity":
            if self.kept_kv is None:
                self.W = np.array([np.eye(self.Nobs) for v in range(self.Nv)])
            else:
                self.W = np.array(
                    [np.eye(self.Nobs) for v in range(len(self.kept_P[comp]))]
                )
        elif type_weight == "KV_sqrt":
            self.Gram_matrix_sqrt(eps=eps_sqrt)
            if self.kept_kv is None:
                self.W = np.array([self.K_sqrt[v] for v in range(self.Nv)])
            else:
                self.W = self.K_sqrt[self.kept_kv[:, comp]]
        else:
            raise NameError("Specified regularization weight type is unknown")

    def learn_metamodel(
        self,
        mu,
        iter_max=200,
        eps_res=1e-6,
        learning_rate=1e-3,
        flag_print=False,
        type_weight="identity",
        eps_sqrt=1e-8,
    ):
        """
        method that performes the offline stage
        optimizes the coefficients of the metamodel using the group LASSO algo
        the output coefficients and the loss vs the learning iterations are stored
        - Input :
            * self: a object from the metamodel class
            * mu: weight coefficient of the group LASSO regularization
            * iter_max: stopping criteria, maximal number of iteration of the optimization algo, by default =200
            * eps_res: stopping criteria, tolerance of the relative residual || Y - Y_hat ||_2 / ||Y||_2, by default =1e-6
            * learning_rate: hyperparameter, learning rate of the optimization process, by default =1e-3
            * flag_print: Boolea, whether or not the details of the optimization process are printed at each iterations, by default =False
            * eps_sqrt : regularization parameter for matrix square root computation

        """

        if isinstance(mu, float) or isinstance(mu, int):
            mu = {self.bact: {self.compound_name[i]: mu for i in range(self.Ncompound)}}

        self.gen_Wv(type_weight=type_weight, eps_sqrt=eps_sqrt)
        res = []
        Nv, Nobs, _ = np.shape(self.K)
        for i in range(self.Ncompound):
            if (
                self.metamodel_yml is not None
                and "identity" in self.metamodel_yml[self.bact]
                and self.compound_name[i] in self.metamodel_yml[self.bact]["identity"]
            ):
                res.append((np.zeros((Nv, Nobs)), 0, [0]))
            else:
                res.append(
                    solve_groupLASSO(
                        self.Flux_learn[:, i],
                        self.K,
                        self.W,
                        mu=mu[self.bact][self.compound_name[i]],
                        iter_max=iter_max,
                        eps_res=eps_res,
                        learning_rate=learning_rate,
                        flag_print=flag_print,
                    )
                )
        res = np.array(res, dtype=object)
        self.theta = np.array([res[i, 0] for i in range(self.Ncompound)])
        # theta in right shape for batch prediction
        self.theta = np.swapaxes(self.theta, 0, 2)
        self.theta_0 = np.array([res[i, 1] for i in range(self.Ncompound)])
        self.learning_loss = [res[i, 2] for i in range(self.Ncompound)]
        length = max(map(len, self.learning_loss))
        curr = np.nan * np.ones((self.Ncompound, length))
        for i_x, xi in enumerate(self.learning_loss):
            curr[i_x, : len(xi)] = xi
        self.learning_loss = curr

    def save_results(self, outfile):
        """Method to save the obtained parameters and loss after optimization."""
        if self.kept_kv is None:  # matrix already dense
            np.savez_compressed(
                outfile, theta=self.theta, theta_0=self.theta_0, loss=self.learning_loss
            )
        else:
            theta = np.zeros((self.Nobs, self.Nv, self.Ncompound))
            for i in range(self.Ncompound):
                theta[:, self.kept_kv[:, i], i] = self.theta[i]
            np.savez_compressed(outfile, theta=theta, theta_0=self.theta_0, loss=np.nan)

    def check_metamodel_compounds_order(self, experiment):
        """
        Check whether the storage order of compounds is similar as the order of
        storage produced by ExperimentSpec.
        """

        assert (
            experiment.compound_name == self.compound_name
        ), "Compounds order does not match"


def flux_metamodel_Wrapper(constraint, bacteria_id, dict_mm):
    """
    method that returns the fluxes estimated from the metamodel of a bacteria
    the fluxes computed using the method flux_estimate of the Metamodel class.
    - Input :
        * constraint: numpy vector of size n_substrate_metabolites, ordered in the same order as provided by experiment.y_id method.
        * bacteria_id: string, name of the bacteria
        * dict_mm: dictionary of metamodel
    - Output :
        * mm_flux: the flux estimated by the metamodel
    """
    if callable(getattr(dict_mm[bacteria_id], "flux_estimate", None)):
        return dict_mm[bacteria_id].flux_estimate(constraint), "META"
    elif callable(getattr(dict_mm[bacteria_id], "predict", None)):
        return dict_mm[bacteria_id].predict(constraint), "META"
    else:
        raise NameError(
            'The method used in flux_metamodel_Wrapper should be named "flux_estimate" or "predict"'
        )
