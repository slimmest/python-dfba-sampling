"""
Create dictionary with all necessary informations.
"""

import os

import cobra
import numpy as np
import yaml


def Create_parameter_dict_from_YAML(configfile, networks=[]):
    """
    From a YAML config file, create a parameter dict.
    - input:
        * configfile: str, path to YAML file
        * networks: list, paths to SBML network files not specified in YAML file

    - output:
        * param, dictionary.
    """

    with open(configfile, "r") as stream:
        param = yaml.safe_load(stream)

    Get_nutritional_environment_variables(param)

    species = set(param["species"].keys())

    # Collect network filenames specified in YAML files.
    yaml_networks = [
        param["species"][s]["sbml_model"]
        for s in species
        if "sbml_model" in param["species"][s]
    ]
    # Read SBML models specified in YAML files or explicitly in networks.
    param["model_dict"] = {
        model.id: model
        for model in [
            cobra.io.read_sbml_model(n)
            for n in yaml_networks + networks
            if os.path.isfile(n)
        ]
    }

    id_models = set(param["model_dict"].keys())

    assert species == id_models, "Id models does not match species list"
    return param


def Get_nutritional_environment_variables(param):
    """
    This function defines, from the nutritional environment definition,
    environment variables (dimensions, variables ids...) necessary for the solver
    - input:
        * param: dictionary of environment definition

    - output:
        None
    """

    # List of all the compounds tracked with ODE (i.e. limiting substrate + bacteria)
    param["compound_name"] = param["Substrate_metabolites"].copy()
    param["compound_name"].update(param["Output_metabolites"])
    param["compound_name"].update(param["species"])

    # Indexing unknowns
    param["Y_id"] = {
        func: func_id for func_id, func in enumerate(param["compound_name"])
    }

    param["id_species"] = np.array([param["Y_id"][b] for b in param["species"]])
    param["id_subs"] = np.array(
        [param["Y_id"][b] for b in param["Substrate_metabolites"]]
    )
    param["id_prod"] = np.array([param["Y_id"][b] for b in param["Output_metabolites"]])

    # Retrieve initial conditions for species & metabolites
    param["y0"] = np.zeros(len(param["Y_id"]))
    for idx, val in zip(param["Y_id"].keys(), param["Y_id"].values()):
        param["y0"][val] = param["compound_name"][idx]["initial_value"]

    # Get global substrate & output reaction map
    param["Substrate_Reaction_Map"] = {}
    param["Output_Reaction_Map"] = {}
    for s in param["species"].keys():
        param["Substrate_Reaction_Map"][s] = param["species"][s][
            "Substrate_Reaction_Map"
        ]
        param["Output_Reaction_Map"][s] = param["species"][s]["Output_Reaction_Map"]
        param["species"][s].pop("Substrate_Reaction_Map", None)
        param["species"][s].pop("Output_Reaction_Map", None)

    # Mapping substrates to the bacteria metabolizing them

    param["Substrate_Bacteria_Map"] = {}
    for bact in param["Substrate_Reaction_Map"].keys():
        for met in param["Substrate_Reaction_Map"][bact].keys():
            if met not in param["Substrate_Bacteria_Map"].keys():
                param["Substrate_Bacteria_Map"][met] = []
            param["Substrate_Bacteria_Map"][met].append(bact)

    # intrinsic flux can be different for all species/output/resources
    param["intrinsic_flux"] = param["intrinsic_flux"] * np.ones(len(param["Y_id"]))
