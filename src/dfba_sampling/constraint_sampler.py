# -*- coding: utf-8 -*-

import gzip
import pickle
from pathlib import Path  # noqa: F401

import numpy as np
import pandas as pd
import yaml

from dfba_sampling.experiment import ExperimentSpec
from dfba_sampling.FBA import FBA_model_Wrapper


class Constraint_Sampler(object):
    def __init__(self, exp_config=None, sample_config=None):
        """init."""
        if sample_config is not None:
            with open(sample_config, "r") as stream:
                self.param = yaml.safe_load(stream)

        if exp_config is not None:
            self.experiment = ExperimentSpec(config=exp_config)

        self.rng = np.random.default_rng(self.param["random_state"])

    def sample_yaml_files(self):
        """
        Create the different yaml files with sampled initial conditions.
        """
        y0 = self.uniform_y0(self.param["N_y0"])
        for i in range(self.param["N_y0"]):
            self.experiment.y0 = y0[i]
            Path(self.path_to_directory(i)).mkdir(parents=True, exist_ok=True)
            self.experiment.save(self.path_to_directory(i) / "config.yml")

    def subsampling(self, df):
        """
        Subample the time-resolved constraints/fluxes generated
        up to a certain fraction. Setting the same random state
        for each function call ensure that the same indexes are
        picked for the constraints and the fluxes.
        """

        return df.sample(
            frac=self.param["subsampling_ratio"],
            random_state=self.param["random_state"],
        )

    def remove_duplicates(self, df_x, df_y):
        """
        Remove the duplicate constraint vectors and associated fluxes, or
        similar vectors up to a threshold epsilon (not yet implemented).
        """
        _, indexes = np.unique(
            [tuple(row) for row in df_x.values], axis=0, return_index=True
        )
        df_x_index = df_x.index[indexes]
        return df_x.loc[df_x_index], [df_y[j].loc[df_x_index] for j in range(len(df_y))]

    def ensure_negative_uptake(self, df):
        """
        Ensure all sampled uptake constraints are nonnegative.
        """
        return df[(df <= 0).all(axis=1)]

    def FBA_from_perturbed_constraints(self, df_x):
        """
        Compute FBA from sampled perturbed constraints.
        """
        df_y_perturb = np.zeros(
            [
                len(self.experiment.species),
                len(df_x),
                len(self.experiment.compound_name),
            ]
        )
        for i, row in enumerate(df_x.values):
            for j, s in enumerate(self.experiment.species):
                fluxes, _ = FBA_model_Wrapper(row[None, :], s, self.experiment)
                df_y_perturb[j, i, :] = fluxes
        return df_y_perturb

    def read_csv(self, i):
        """
        read output result in csv format
        """
        df_x = pd.read_csv(
            self.path_to_directory(i) / "out_constraint.tsv", header=0, comment="#"
        )
        df_y = [
            pd.read_csv(
                self.path_to_directory(i) / f"out_FBA_{s}.tsv",
                header=0,
                comment="#",
            )
            for s in self.experiment.species
        ]
        return df_x, df_y

    def read_pickle(self, i):
        """
        read output result in pickle format
        """
        with gzip.open(self.path_to_directory(i) / "default", "rb") as f:
            A = pickle.load(f)
        df_x = A["constraint"]
        df_y = [A["fba_matrix"][s] for s in self.experiment.species]
        return df_x, df_y

    def assemble(self, file_format="csv"):
        """
        Assemble the database.
        """
        self.database_x = pd.DataFrame(columns=self.experiment.substrate_metabolites)
        # One vector Y by species
        self.database_y = [
            pd.DataFrame(columns=self.experiment.compound_name)
            for j in range(len(self.experiment.species))
        ]
        # Iterating over N_y0, the number of different initial conditions.
        for i in range(self.param["N_y0"]):
            if file_format == "csv":
                df_x, df_y = self.read_csv(i)
            elif file_format == "pickle":
                df_x, df_y = self.read_pickle(i)
            else:
                raise NameError(
                    'Unknown output file format. Should be "csv" or "pickle"'
                )
            df_x.drop("t", axis=1, inplace=True)
            # drops substrate with positive lower bound
            df_x = self.ensure_negative_uptake(df_x)
            for j in range(len(df_y)):
                df_y[j].drop("t", axis=1, inplace=True)
                df_y[j] = df_y[j].loc[df_x.index]

            df_x = self.subsampling(df_x)
            df_y = [self.subsampling(df_y[j]) for j in range(len(df_y))]

            df_x, df_y = self.remove_duplicates(df_x, df_y)

            df_x_perturb = self.perturb_constraint(df_x)
            df_y_perturb = self.FBA_from_perturbed_constraints(df_x_perturb)

            # append perturbed and non-perturbed flux
            df_x = pd.concat([df_x, df_x_perturb])
            self.database_x = pd.concat([self.database_x, df_x])

            # assemble FBA results for pertubed and non-pertubed flux.
            for j in range(len(self.database_y)):
                df_y[j] = pd.concat(
                    [
                        df_y[j],
                        pd.DataFrame(
                            df_y_perturb[j], columns=self.experiment.compound_name
                        ),
                    ],
                    ignore_index=True,
                )
                self.database_y[j] = pd.concat([self.database_y[j], df_y[j]])

        self.save_database()

    def perturb_constraint(self, df):
        """
        Perturb constraints using multiplicative gaussian noise
        and ensure that the perturbed uptake constraints are negative.
        """
        return self.ensure_negative_uptake(
            df.multiply(
                self.rng.normal(0, self.param["perturb"]["sigma"], size=df.shape)
            )
        )

    def uniform_y0(self, N):
        """
        Create the uniform probability distribution
        to sample initial conditions. Only handles
        uniform law so far.
        """
        # if self.param["uniform_y0"]["log"]: not yet implemented
        n_metabolites = len(self.experiment.y0) - len(self.experiment.species)
        lb = self.param["uniform_y0"]["bounds"][0] * np.ones(n_metabolites)
        ub = self.param["uniform_y0"]["bounds"][1] * np.ones(n_metabolites)
        lb_bact = np.ones(len(self.experiment.species))
        ub_bact = np.ones(len(self.experiment.species))
        for i, spec in enumerate(self.experiment.species):
            if isinstance(self.param["uniform_y0"]["bounds_bact"], dict):
                lb_bact[i] = self.param["uniform_y0"]["bounds_bact"][spec][0]
                ub_bact[i] = self.param["uniform_y0"]["bounds_bact"][spec][1]
            elif isinstance(self.param["uniform_y0"]["bounds_bact"], list):
                lb_bact[i] = self.param["uniform_y0"]["bounds_bact"][0]
                ub_bact[i] = self.param["uniform_y0"]["bounds_bact"][1]
            else:
                raise NameError("Wrong input type for bounds_bact")
        concentration_metabolites = self.rng.uniform(lb, ub, size=(N, len(lb)))
        concentration_bact = self.rng.uniform(lb_bact, ub_bact, size=(N, len(lb_bact)))

        # only metabolites can be present/absent
        presence = self.rng.choice(
            range(2),
            size=(N, n_metabolites),
            p=[1 - self.param["proba_presence_y0"], self.param["proba_presence_y0"]],
        )

        concentration_metabolites *= presence
        return np.c_[concentration_metabolites, concentration_bact]

    def save_database(self):
        """
        Save the generated learning database in tsv files.
        """
        self.database_x.to_csv(
            Path(f"{self.param['path_to_directories']}/database_constraints.tsv"),
            index=False,
            sep="\t",
        )
        [
            self.database_y[j].to_csv(
                Path(f"{self.param['path_to_directories']}/database_fluxes_{s}.tsv"),
                index=False,
                sep="\t",
            )
            for j, s in enumerate(self.experiment.species)
        ]

    def path_to_directory(self, n):
        """
        Return the path to a specific directory
        """
        return (
            Path(f"{self.param['path_to_directories']}")
            / f"{self.param['directories_prefix']}_{n}"
        )
