"""
Module that contains the command line app.

Why does this file exist, and why not put this in __main__?

  You might be tempted to import things from __main__ later, but that will cause
  problems: the code will get executed twice:

  - When you run `python -mdfba_sampling` python will execute
    ``__main__.py`` as a script. That means there won't be any
    ``dfba_sampling.__main__`` in ``sys.modules``.
  - When you import __main__ it will get executed again (as a module) because
    there's no ``dfba_sampling.__main__`` in ``sys.modules``.

  Also see (1) from http://click.pocoo.org/5/setuptools/#setuptools-integration
"""

import argparse
import sys

from dfba_sampling.dFBA import TimeIntegration
from dfba_sampling.experiment import ExperimentResult
from dfba_sampling.experiment import ExperimentSpec
from dfba_sampling.utils import assure_valid_directory
from dfba_sampling.vizu import Plot
from dfba_sampling.vizu import PlotFBA

parser = argparse.ArgumentParser(description="Command description.")
parser.add_argument(
    "-n",
    "--networks",
    nargs=argparse.ZERO_OR_MORE,
    help="Metabolic networks described as SBML files.",
    type=str,
)
parser.add_argument(
    "-c",
    "--config",
    help="dFBA parameters, initial conditions, exchange reactions (YAML)",
    type=str,
)
parser.add_argument(
    "-o", "--output", help="Path to output folder", type=assure_valid_directory
)


def main(args=None):
    try:
        args = parser.parse_args(args=args)
    except OSError:
        print("Output directory invalid")
        sys.exit(1)

    experiment = ExperimentSpec(config=args.config, networks=args.networks)
    result_y, result_fba, result_constraint = TimeIntegration(experiment)
    results = ExperimentResult(experiment, result_y, result_constraint, result_fba)
    results.save(args.output)
    Plot(result_y, experiment, folder_name=args.output)
    PlotFBA(result_constraint, result_fba, experiment, folder_name=args.output)
