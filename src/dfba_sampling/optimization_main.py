# -*- coding: utf-8 -*-
import argparse
import ast
import os
import sys
from pathlib import Path

from dfba_sampling.metamodel import Metamodel
from dfba_sampling.utils import assure_valid_directory
from dfba_sampling.utils import save_learning_output

parser = argparse.ArgumentParser(description="Command description.")
parser.add_argument(
    "-d",
    "--data",
    nargs=argparse.ZERO_OR_MORE,
    help="Database constraints and fluxes",
    type=str,
)
parser.add_argument(
    "-p",
    "--params",
    help="dict of algorithm parameters: regularization, learning rate, niter, nobs",
    type=ast.literal_eval,
)

parser.add_argument(
    "-g",
    "--gram",
    help="Precomputed gram matrix",
    type=str,
)

parser.add_argument(
    "-o", "--output", help="Path to output folder", type=assure_valid_directory
)


def main(args=None):
    try:
        args = parser.parse_args(args=args)
    except OSError:
        print("Output directory invalid")
        sys.exit(1)

    for k, v in args.params.items():
        args.params[k] = int(v) if k in ["n_obs", "n_iter"] else float(v)

    mm = Metamodel(constraints=args.data[0], fluxes=args.data[1], kernel_type="Matern")

    mm.P_MaxOrder(3)
    mm.load_attributes(gram_matrix=args.gram)
    # mm.GramMatrix()

    mm.learn_metamodel(
        args.params["mu"],
        iter_max=args.params["n_iter"],
        eps_res=args.params["eps_res"],
        flag_print=True,
        learning_rate=args.params["learning_rate"],
    )
    path = Path(args.output) / f"mu_{args.params['mu']}"
    try:
        os.mkdir(path)
    except FileExistsError:
        pass
    save_learning_output(mm, path)


main()
