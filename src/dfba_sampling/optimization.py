#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 11:59:21 2021

@author: malou
"""

# import metamodel as metamod
import numpy as np
import numpy.linalg as npl


def solve_groupLASSO(
    Y,
    K,
    W,  # theta, theta_0,
    mu=0,
    iter_max=200,
    eps_res=1e-6,
    learning_rate=1e-3,
    flag_print=True,
):
    """
    method that solves the groupe LASSO optimization problem
    theta^hat, theta_0^hat = argmin 1/2|| Y-( theta_0 I_n + sum_v K_v . theta_v )||^2_2
    + mu sum_v || W_v . theta_v )||_2
    - Input :
        * Y: vector of size Nobs containing the observed fluxes
        * K: array of size Nv x Nobs x Nobs containing the Nv Gram matrices
        * W: array of size Nv x Nobs x Nobs containing the weight matrices for each part of the sparsity regularization
        * mu: hyperparameter, weight coefficient of the sparsity regularization, by default =0
        * iter_max: stopping criteria, maximal number of iteration of the optimization algo, by default =200
        * eps_res: stopping criteria, tolerance of the relative residual || Y - Y_hat ||_2 / ||Y||_2
        * learning_rate: hyperparameter, learning rate of the optimization process, by default =1e-3
        * flag_print: Boolea, whether or not the details of the optimization process are printed at each iterations, by default =True
    - Output :
        * theta^hat: the optimal coefficents
        * theta_0^hat: the optimal intercept
        * res_array: the array that contains the residual vs the optimization iterations

    """
    Nv, Nobs, _ = np.shape(K)
    theta = np.zeros((Nv, Nobs))
    theta_0 = 0
    nb_iter = 0

    res_array = -1 * np.ones((iter_max + 1, 1))
    if npl.norm(Y) == 0:
        res = 0
    else:
        res = residual_fct(Y, K, theta, theta_0)
    res_array[nb_iter] = res
    # learning_rate = 1.0 / 200.0  # 1./20000000. #
    while nb_iter < iter_max and res > eps_res:
        # print("test enter loop optim")
        theta_0 = np.mean(
            Y - np.sum([K[v, :, :].dot(theta[v, :]) for v in range(Nv)], axis=0)
        )
        for v in range(Nv):
            Rv = (
                Y
                - theta_0
                - np.sum(
                    [K[w, :, :].dot(theta[w, :]) for w in range(Nv) if w != v], axis=0
                )
            )
            if npl.norm(2 * W[v, :, :].dot(Rv) / np.sqrt(Nobs)) <= mu:
                theta[v, :] = 0
            else:
                # pb avec l initialisation à 0 qui implique la reg va a +infty
                # pb contourner en utilisant une initialisation random et un moindre carré classique en cas de probleme
                if npl.norm(theta[v, :]) == 0:
                    if flag_print:
                        print(
                            "at iteration %03d, the %02d th part has all theta =0 at the previous iteration which is corrected"
                            % (nb_iter, np.floor(v))
                        )
                    theta[v, :] -= learning_rate * (
                        -2 * K[v, :, :].dot(Rv - K[v, :, :].dot(theta[v, :]))
                    )
                grad_J = -2 * K[v, :, :].dot(
                    Rv - K[v, :, :].dot(theta[v, :])
                ) + np.sqrt(Nobs) * mu * W[v, :, :].dot(W[v, :, :]).dot(
                    theta[v, :]
                ) / npl.norm(
                    W[v, :, :].dot(theta[v, :])
                )
                theta[v, :] -= learning_rate * grad_J
        res = residual_fct(Y, K, theta, theta_0)
        nb_iter += 1
        res_array[nb_iter] = res
        if flag_print:
            print(
                "### iteration %03d / " % (nb_iter),
                iter_max,
                ", residual={:.2e}".format(res),
                "% of ||Y||_2/sqrt(Nobs)",
            )
    return theta, theta_0, res_array[res_array >= 0]
    # return res_array[res_array >= 0]


def error_fct(Y, Y_hat):
    return npl.norm(Y - Y_hat) / npl.norm(Y)


def residual_fct(Y, K, theta, theta_0):
    Y_hat = theta_0 + np.sum(
        [K[v, :, :].dot(theta[v, :]) for v in range(np.shape(K)[0])], axis=0
    )
    return error_fct(Y, Y_hat)
