# -*- coding: utf-8 -*-
import numpy as np

from dfba_sampling.FBA import FBA_model_Wrapper
from dfba_sampling.metamodel import flux_metamodel_Wrapper


def Constraint_definition(Y, experiment, eps=1e-14):
    """
    This function, from the current concentrations stored in the concentration vector Y,
    computes the FBA constraints given the parameter param.
    - Input :
        * Y: numpy array
        * experiment: object of the Experiment class
    - Output :
        * C: dictionnary mapping substrate to their constraint

    """
    if Y.ndim == 2:
        constraint = np.zeros([Y.shape[0], len(experiment.substrate_metabolites)])
        Y_in = Y
    elif Y.ndim == 1:
        constraint = np.zeros([1, len(experiment.substrate_metabolites)])
        Y_in = Y.reshape([1, Y.shape[0]])
    else:
        raise NameError("Wrong input dimension in Constraint definition")
    for met in experiment.substrate_metabolites:
        curr_metabolizing_bacteria = [
            experiment.y_id(bact) for bact in experiment.substrate_species(met)
        ]
        y_bact = np.sum(
            Y_in[:, curr_metabolizing_bacteria], axis=1
        )  # compute total biomass of bacteria metabolizing the current substrate
        constraint[:, experiment.y_id(met)] = np.maximum(
            -Y_in[:, experiment.y_id(met)] / (experiment.limit_dt * y_bact + eps),
            experiment.intrinsic_flux[experiment.y_id(met)],
        )
        if (
            np.any(Y_in[:, experiment.y_id(met)] / (experiment.limit_dt * y_bact + eps))
            < 0
        ):
            print(
                "Warning! invalid value in FBA solver. Negative density founds for metabolite "
                + met
                + ". Y value "
                + str(np.min(Y[:, experiment.y_id(met)]))
                + ". y_bact value "
                + str(y_bact)
            )
    return constraint


def TimeIntegration(experiment, RHS=FBA_model_Wrapper, dict_mm=None, Total_FLUX=False):
    """
    Perform a time integration of the system

    - Input :
        * experiment: object of the Experiment class
        * RHS: callable to compute the fluxes.
        * dict_mm: dictionary of metamodels for statistical estimate of fluxes.

    - Output:
        * result_y: numpy array with time+current_state
        * result_fba: numpy array of the FBA results along time integration (FBA outputs)
        * result_constraint: numpy array of FBA constraints along time integration (FBA inputs)
        * total_flux: dictionary of dictionaries of all reaction fluxes at each timestep
    """

    # defining the RHS only once, there must be a prettier way to do this.
    if RHS == FBA_model_Wrapper:

        def new_RHS(constraint, bacteria_id, experiment, dict_mm):
            return FBA_model_Wrapper(
                constraint, bacteria_id, experiment, Total_FLUX=Total_FLUX
            )

    else:

        def new_RHS(constraint, bacteria_id, experiment, dict_mm):
            return flux_metamodel_Wrapper(constraint, bacteria_id, dict_mm)

    nrows = experiment.nb_timestep
    nb_species = len(experiment.species)
    nb_compound = len(experiment.compound_name)
    ncols = 1 + nb_compound  # first column stands for the time.
    ncols_constraints = 1 + len(experiment.substrate_metabolites)

    y = experiment.y0.copy()
    result_y = np.zeros([nrows, ncols])
    result_y[0, 1:] = y
    result_constraint = np.zeros([nrows, ncols_constraints])
    result_flux = np.zeros([nb_species, nrows, ncols])
    curr_flux = np.zeros([nb_species, ncols - 1])
    if Total_FLUX:
        total_flux = (
            {}
        )  # dict to store dictionaries of all reaction fluxes at each timestep

    for i in range(nrows):
        t = i * experiment.time_step
        t_dfba = (i + 1) * experiment.time_step  # dFBA starts at .01 in testfiles...
        if i % 100 == 0:
            print("t=", t, "/", experiment.time, "hours")

        flux_fba = np.zeros((nb_compound,))
        constraint = Constraint_definition(y, experiment)
        result_constraint[i, :] = np.append(
            t,
            [
                constraint[:, experiment.y_id(met)]
                for met in experiment.substrate_metabolites
            ],
        )

        if Total_FLUX:
            timestep_fluxes = {}  # Dictionary to store fluxes at this timestep

        for j, bacteria_id in enumerate(experiment.species):
            res = new_RHS(constraint, bacteria_id, experiment, dict_mm)
            curr_flux[j] = res[0]
            result_flux[j, i, :] = np.append(t, curr_flux[j])
            flux_fba += curr_flux[j] * y[experiment.y_id(bacteria_id)]

            if Total_FLUX:
                # Store all species fluxes for this timestep
                timestep_fluxes[bacteria_id] = res[2]

        if Total_FLUX:
            # Add all fluxes at this timestep
            total_flux[i] = timestep_fluxes

        id_pos = flux_fba >= 0.0
        id_neg = flux_fba < 0.0
        if (y[id_neg] > 0.0).all():
            y[id_neg] = y[id_neg] / (
                1 - experiment.time_step * flux_fba[id_neg] / y[id_neg]
            )
        else:
            wrong_state = y[id_neg] == 0.0
            raise NameError(
                "A depleted metabolite is still consumed."
                + "Depleted metabolites :"
                + " ".join([experiment.compound_name[id_w] for id_w in wrong_state])
            )

        y[id_pos] = y[id_pos] + experiment.time_step * flux_fba[id_pos]

        if i < nrows - 1:
            result_y[i + 1] = np.append(t_dfba, y)

    if Total_FLUX:
        return result_y, result_flux, result_constraint, total_flux
    else:
        return result_y, result_flux, result_constraint


# def FBA_model_Wrapper(constraint, bacteria_id, experiment):
#     """
#     This function maps constraints to the corresponding FBA fluxes for
#     the bacteria and metabolites screened in the dFBA
#     - Input:
#         * constraint: dictionnary
#         * bacteria_id : string
#         * experiment: object of the Experiment class
#     - Output:
#         * F: numpy array of fluxes (F.shape = (len(param['compound_name']),))
#         * flag_err : boolean . Indicates if FBA failed (infeasible status)
#     """
#     pfba_sol = Mock_sol(experiment, bacteria_id)
#     optimal_flux = np.zeros((constraint.shape[0], len(experiment.compound_name)))
#     flag_negative_constraint = True
#     flag_err = ""
#     for i_constraint in range(constraint.shape[0]):
#         with experiment.models[bacteria_id] as mod:
#             for (
#                 met
#             ) in (
#                 experiment.substrate_metabolites
#             ):  # for each limiting substrate, we define the new lower bound
#                 if met in experiment.substrate_reaction(bacteria_id):
#                     reac_mod = mod.reactions.get_by_id(
#                         experiment.substrate_reaction(bacteria_id)[met]
#                     )
#                     try:
#                         reac_mod.lower_bound = constraint[
#                             i_constraint, experiment.y_id(met)
#                         ]
#                     except Exception:
#                         print("Error in constraint access")
#                         pfba_sol.fluxes = {k: 0.0 for k in pfba_sol.fluxes.keys()}
#                         pfba_sol.status = "infeasible"
#                         flag_negative_constraint = False
#             if (
#                 experiment._order_reactions[bacteria_id] is not None
#                 and flag_negative_constraint
#             ):
#                 try:
#                     cp.util.fix_objective_as_constraint(mod)
#                     pfba_sol.fluxes = cp.util.add_lexicographic_constraints(
#                         mod,
#                         experiment._order_reactions[bacteria_id],
#                         ["max" for _ in experiment._order_reactions[bacteria_id]],
#                     )
#                     pfba_sol.status = "optimal"
#                 except Exception:
#                     print("Warning : lexicographic computation failed")
#                     pfba_sol.fluxes = {k: 0.0 for k in pfba_sol.fluxes.keys()}
#                     pfba_sol.status = "infeasible"
#             else:
#                 pfba_sol = mod.optimize()
#         flag_err = pfba_sol.status
#         if flag_err == "optimal":
#             for met, reaction in experiment.observables(bacteria_id):
#                 optimal_flux[i_constraint, experiment.y_id(met)] = pfba_sol.fluxes[
#                     reaction
#                 ]
#         else:
#             # optimal_flux = np.zeros((len(experiment.compound_name),))
#             flag_err = f"Constraint {constraint[i_constraint]} led to model infeasible in {bacteria_id}"
#             print(flag_err)
#     if constraint.shape[0] == 1:
#         optimal_flux = optimal_flux.reshape((len(experiment.compound_name),))
#     return optimal_flux, flag_err

# def TimeIntegration(experiment, RHS=FBA_model_Wrapper, dict_mm=None):
#     """
#     Perform a time integration of the system

#     - Input :
#         * experiment: object of the Experiment class
#         * RHS: callable to compute the fluxes.
#         * dict_mm: dictionary of metamodels for statistical estimate of fluxes.

#     - Output:
#         * result_y: numpy array with time+current_state
#         * result_fba: numpy array of the FBA results along time integration (FBA outputs)
#         * result_constraint: numpy array of FBA constraints along time integration (FBA inputs)
#     """

#     # defining the RHS only once, there must be a prettier way to do this.
#     if RHS == FBA_model_Wrapper:

#         def new_RHS(constraint, bacteria_id, experiment, dict_mm):
#             return FBA_model_Wrapper(constraint, bacteria_id, experiment)

#     else:
#         # [dict_mm[k].check_metamodel_compounds_order(experiment) for k in dict_mm.keys()]

#         def new_RHS(constraint, bacteria_id, experiment, dict_mm):
#             return flux_metamodel_Wrapper(constraint, bacteria_id, dict_mm)

#     nrows = experiment.nb_timestep
#     nb_species = len(experiment.species)
#     nb_compound = len(experiment.compound_name)
#     ncols = 1 + nb_compound  # first column stands for the time.
#     ncols_constraints = 1 + len(experiment.substrate_metabolites)

#     y = experiment.y0.copy()
#     result_y = np.zeros([nrows, ncols])
#     result_y[0, 1:] = y
#     result_constraint = np.zeros([nrows, ncols_constraints])
#     result_flux = np.zeros([nb_species, nrows, ncols])
#     curr_flux = np.zeros([nb_species, ncols - 1])

#     for i in range(nrows):
#         t = i * experiment.time_step
#         t_dfba = (i + 1) * experiment.time_step  # dFBA starts at .01 in testfiles...
#         if i % 100 == 0:
#             print("t=", t, "/", experiment.time, "hours")

#         flux_fba = np.zeros((nb_compound,))
#         constraint = Constraint_definition(y, experiment)
#         result_constraint[i, :] = np.append(
#             t,
#             [
#                 constraint[:, experiment.y_id(met)]
#                 for met in experiment.substrate_metabolites
#             ],
#         )

#         for j, bacteria_id in enumerate(experiment.species):
#             curr_flux[j], flag_err = new_RHS(
#                 constraint, bacteria_id, experiment, dict_mm
#             )
#             result_flux[j, i, :] = np.append(t, curr_flux[j])
#             flux_fba += curr_flux[j] * y[experiment.y_id(bacteria_id)]
#             # semi implicit Euler scheme for ODE integration to preserve solution sign

#             # for susbstrate, that are consumed:
#             # if flux is negative:
#             # (y_sub^{n+1}-y_sub^{n})/dt = Flux_fba_sub*y_sub^{n+1}/y_sub^{n}
#             # => y_sub^{n+1} =  y_sub^{n} /(1-dt*Flux_fba_sub/y_sub^{n})
#             # else:
#             # y_sub^{n+1} =  y_sub^{n} + dt*Flux_fba_sub
#         id_pos = flux_fba >= 0.0
#         id_neg = flux_fba < 0.0
#         if (y[id_neg] > 0.0).all():
#             y[id_neg] = y[id_neg] / (
#                 1 - experiment.time_step * flux_fba[id_neg] / y[id_neg]
#             )
#         else:
#             wrong_state = y[id_neg] == 0.0
#             raise NameError(
#                 "A depleted metabolite is still consumed."
#                 + "Depleted metabolites :"
#                 + " ".join([experiment.compound_name[id_w] for id_w in wrong_state])
#             )

#         y[id_pos] = y[id_pos] + experiment.time_step * flux_fba[id_pos]

#         if i < nrows - 1:
#             result_y[i + 1] = np.append(t_dfba, y)

#     return result_y, result_flux, result_constraint
